#!/usr/bin/env python


import paho.mqtt.client as mqtt
import os
import ssl
import time

broker = os.environ.get("MQTT_HOST", "mqtt.localhost")
port = os.environ.get("MQTT_PORT", 1883)
#tls_ca = os.environ.get("MQTT_TLS_CA", "tls/ca.crt")
user = os.environ.get("MQTT_USER", "")
pwd = os.environ.get("MQTT_PWD", "")
topic = os.environ.get("MQTT_TOPIC", "decenter/reservations/confirmed")

client = mqtt.Client()
#client.tls_set(tls_ca, tls_version=ssl.PROTOCOL_TLSv1_2)
client.username_pw_set(user, pwd)

client.connect(broker, port)

time.sleep(1)
print("publishing")
client.publish(topic, "10f90f148-8154-4b01-90e6-d701748f0881")
client.disconnect()
