#!/usr/bin/env bash
MQTT_CFG_FILE=${MQTT_CFG_FILE:-$PWD/mosquitto.conf}

echo "Running mosquitto with values"
echo -e "\t MQTT_CFG_FILE=\t$MQTT_CFG_FILE"
docker run --rm -d -p 1883:1883 \
	-v $MQTT_CFG_FILE:/mosquitto/config/mosquitto.conf \
	--name mqtt eclipse-mosquitto
