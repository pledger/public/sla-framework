#!/usr/bin/env python
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0.
#
# The Eclipse Public License is available at
#    http://www.eclipse.org/legal/epl-v10.html
#

# Modified from https://github.com/eclipse/paho.mqtt.python
import paho.mqtt.client as mqtt
import os
import ssl
import time

broker = os.environ.get("MQTT_HOST", "mqtt.localhost")
port = int(os.environ.get("MQTT_PORT", 1883))
#tls_ca = os.environ.get("MQTT_TLS_CA", "tls/ca.crt")
user = os.environ.get("MQTT_USER", "")
pwd = os.environ.get("MQTT_PWD", "")

def on_connect(client, userdata, flags, rc):
    print("Connecting: "+str(rc))
    client.subscribe("decenter/reservations/confirmed")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
#client.tls_set(tls_ca, tls_version=ssl.PROTOCOL_TLSv1_2)
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(user, pwd)

client.connect(broker, port, 60)
client.loop_forever()

