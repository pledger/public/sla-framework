//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package main

import (
	"atos.pledger/sla-framework/generator"
	"atos.pledger/sla-framework/model"
	"atos.pledger/sla-framework/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
	"errors"
	"strings"
	"io"

	log "atos.pledger/sla-framework/common/logs"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

const (
	defaultPort        string = "8090"
	defaultEnableSsl   bool   = false
	defaultSslCertPath string = "cert.pem"
	defaultSslKeyPath  string = "key.pem"

	portPropertyName        = "port"
	enableSslPropertyName   = "enableSsl"
	sslCertPathPropertyName = "sslCertPath"
	sslKeyPathPropertyName  = "sslKeyPath"
)

// App is a main application "object", to be built by main and testmain
type App struct {
	Router      *mux.Router
	Repository  model.IRepository
	Port        string
	SslEnabled  bool
	SslCertPath string
	SslKeyPath  string
	externalIDs bool
	validator   model.Validator
}

// ApiError is the struct sent to client on errors
type ApiError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

/*
Error ...
*/
func (e *ApiError) Error() string {
	return e.Message
}

// endpoint represents an available operation represented by its HTTP method, the expected path for invocations and an optional help message.
type endpoint struct {
	// example: GET
	Method string

	// example: /providers
	Path string

	// example: Gets a list of registered providers
	Help string
}

var api = map[string]endpoint{
	"providers":  endpoint{"GET", "/providers", "Providers"},
	"agreements": endpoint{"GET", "/agreements", "Agreements"},
	"templates":  endpoint{"GET", "/templates", "Templates"},
	"violations": endpoint{"GET", "/violations", "Templates"},
}

func NewApp(config *viper.Viper, repository model.IRepository, validator model.Validator) (App, error) {

	setDefaults(config)
	logConfig(config)

	a := App{
		Port:        config.GetString(portPropertyName),
		SslEnabled:  config.GetBool(enableSslPropertyName),
		SslCertPath: config.GetString(sslCertPathPropertyName),
		SslKeyPath:  config.GetString(sslKeyPathPropertyName),
		externalIDs: config.GetBool(utils.ExternalIDsPropertyName),
		validator:   validator,
	}

	a.initialize(repository)
	/*
	 * TODO Return error if files not found, for ex.
	 */
	return a, nil
}

func setDefaults(config *viper.Viper) {
	config.SetDefault(portPropertyName, defaultPort)
	config.SetDefault(sslCertPathPropertyName, defaultSslCertPath)
	config.SetDefault(sslKeyPathPropertyName, defaultSslKeyPath)
}

func logConfig(config *viper.Viper) {
	ssl := "no"
	if config.GetBool(enableSslPropertyName) == true {
		ssl = fmt.Sprintf(
			"(cert='%s', key='%s')",
			config.GetString(sslCertPathPropertyName),
			config.GetString(sslKeyPathPropertyName))
	}

	log.Printf(pathLOG+"[logConfig] HTTP/S configuration\n"+
		"\tport: %v\n"+
		"\tssl: %v\n",
		config.GetString(portPropertyName),
		ssl)
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

// Initialize initializes the REST API passing the db connection
func (a *App) initialize(repository model.IRepository) {

	a.Repository = repository

	a.Router = mux.NewRouter().StrictSlash(true)

	a.Router.HandleFunc("/", a.Index).Methods("GET")

	// AGREEMENTS:
	a.Router.Methods("GET").Path("/agreements").Handler(logger(a.GetAgreements))
	a.Router.Methods("POST").Path("/agreements").Handler(logger(a.CreateAgreement))
	a.Router.Methods("GET").Path("/agreements/{id}").Handler(logger(a.GetAgreement))
	a.Router.Methods("DELETE").Path("/agreements/{id}").Handler(logger(a.DeleteAgreement))
	a.Router.Methods("PATCH").Path("/agreements/{id}").Handler(logger(a.UpdateAgreement))
	a.Router.Methods("GET").Path("/agreements/{id}/details").Handler(logger(a.GetAgreementDetails))
	a.Router.Methods("POST").Path("/notifications").Handler(logger(a.ReceiveNotification))

	// PROVIDERS:
	a.Router.Methods("GET").Path("/providers").Handler(logger(a.GetAllProviders))
	a.Router.Methods("POST").Path("/providers").Handler(logger(a.CreateProvider))
	a.Router.Methods("GET").Path("/providers/{id}").Handler(logger(a.GetProvider))
	a.Router.Methods("DELETE").Path("/providers/{id}").Handler(logger(a.DeleteProvider))

	// TEMPLATES:
	a.Router.Methods("GET").Path("/templates").Handler(logger(a.GetTemplates))
	a.Router.Methods("GET").Path("/templates/{id}").Handler(logger(a.GetTemplate))
	a.Router.Methods("POST").Path("/templates").Handler(logger(a.CreateTemplate))

	// DEPRECATED: All these PUT below are deprecated, and superseded by PATCH above
	a.Router.Methods("PUT").Path("/agreements/{id}/start").Handler(logger(a.StartAgreement))
	a.Router.Methods("PUT").Path("/agreements/{id}/stop").Handler(logger(a.StopAgreement))
	a.Router.Methods("PUT").Path("/agreements/{id}/terminate").Handler(logger(a.TerminateAgreement))
	a.Router.Methods("PUT").Path("/agreements/{id}").Handler(logger(a.UpdateAgreement))
	a.Router.Methods("POST").Path("/create-agreement").Handler(logger(a.CreateAgreementFromTemplate))

	// OTHERS & HISTORICAL DATA MANAGEMENT
	a.Router.Methods("GET").Path("/violations").Handler(logger(a.GetAllViolations))
	a.Router.Methods("GET").Path("/violations/{id}").Handler(logger(a.GetViolations))

	a.Router.Methods("GET").Path("/historic").Handler(logger(a.GetAllViolations))
	a.Router.Methods("GET").Path("/historic/{id}").Handler(logger(a.GetAppViolations))

	// swagger api
	sh := http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("./swaggerui/")))
	a.Router.PathPrefix("/swaggerui/").Handler(sh)
}

/* 
Run starts the REST API
*/
func (a *App) Run() {
	addr := ":" + a.Port

	log.Info("####################################################################################")
	
	if a.SslEnabled {
		log.Fatal(http.ListenAndServeTLS(addr, a.SslCertPath, a.SslKeyPath, a.Router))
	} else {
		log.Fatal(http.ListenAndServe(addr, a.Router))
	}
}

/* 
Index is the API index
*/
func (a *App) Index(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(api)
}

// logger
func logger(f func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return loggerDecorator(http.HandlerFunc(f))
}

// loggerDecorator
func loggerDecorator(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(pathLOG+
			"[loggerDecorator] %s\t%s\t\t%s",
			r.Method,
			r.RequestURI,
			time.Since(start),
		)
	})
}

// getAll
func (a *App) getAll(w http.ResponseWriter, r *http.Request, f func() (interface{}, error)) {
	list, err := f()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	} else {
		respondSuccessJSON(w, list)
	}
}

// get
func (a *App) get(w http.ResponseWriter, r *http.Request, f func(string) (interface{}, error)) {
	vars := mux.Vars(r)
	id := vars["id"]

	provider, err := f(id)
	if err != nil {
		manageError(err, w)
	} else {
		respondSuccessJSON(w, provider)
	}
}

// create
func (a *App) create(w http.ResponseWriter, r *http.Request, decode func() error, create func() (model.Identity, error)) {
	errDec := decode()
	if errDec != nil {
		log.Error(pathLOG + "[create] Error decoding input: ", errDec.Error())
		respondWithError(w, http.StatusBadRequest, errDec.Error())
		return
	}
	/* check errors */
	created, err := create()
	if err != nil {
		manageError(err, w)
	} else {
		respondWithJSON(w, http.StatusCreated, created)
	}
}

// Update operation where the resource is updated with the body passed in request
func (a *App) updateEntity(w http.ResponseWriter, r *http.Request, decode func() error, update func(id string) (model.Identity, error)) {
	vars := mux.Vars(r)
	id := vars["id"]

	errDec := decode()
	if errDec != nil {
		respondWithError(w, http.StatusBadRequest, errDec.Error())
		return
	}
	/* check errors */
	updated, err := update(id)
	if err != nil {
		manageError(err, w)
	} else {
		respondWithJSON(w, http.StatusOK, updated)
	}
}

// Any other update operation not covered by updateEntity (e.g., delete)
func (a *App) update(w http.ResponseWriter, r *http.Request, upd func(string) error) {
	vars := mux.Vars(r)
	id := vars["id"]

	err := upd(id)

	if err != nil {
		manageError(err, w)
	} else {
		respondNoContent(w)
	}
}

/* 
GetAllProviders return all providers in db
*/
func (a *App) GetAllProviders(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.getAll(w, r, func() (interface{}, error) {
		return a.Repository.GetAllProviders()
	})
}

/* 
GetProvider gets a provider by REST ID
*/
func (a *App) GetProvider(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.get(w, r, func(id string) (interface{}, error) {
		return a.Repository.GetProvider(id)
	})
}

/* 
CreateProvider creates a provider passed by REST params
*/
func (a *App) CreateProvider(w http.ResponseWriter, r *http.Request) {

	var provider model.Provider

	a.create(w, r,
		func() error {
			return json.NewDecoder(r.Body).Decode(&provider)
		},
		func() (model.Identity, error) {
			return a.Repository.CreateProvider(&provider)
		})
}

/* 
DeleteProvider deletes /provider/id
*/
func (a *App) DeleteProvider(w http.ResponseWriter, r *http.Request) {
	a.update(w, r, func(id string) error {
		return a.Repository.DeleteProvider(&model.Provider{Id: id})
	})
}

/* 
GetAgreements return all agreements in db
*/
func (a *App) GetAgreements(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	v := r.URL.Query()
	active := v.Get("active")

	a.getAll(w, r, func() (interface{}, error) {
		if active != "" {
			return a.Repository.GetAgreementsByState(model.STARTED)
		}
		return a.Repository.GetAllAgreements()
	})
}

/* 
GetAgreement gets an agreement by REST ID
*/
func (a *App) GetAgreement(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.get(w, r, func(id string) (interface{}, error) {
		return a.Repository.GetAgreement(id)
	})
}

/* 
GetAgreementDetails gets an agreement by REST ID
*/
func (a *App) GetAgreementDetails(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.get(w, r, func(id string) (interface{}, error) {
		agreement, error := a.Repository.GetAgreement(id)
		return agreement.Details, error
	})
}

/* 
CreateAgreement creates a agreement passed by REST params
*/
func (a *App) CreateAgreement(w http.ResponseWriter, r *http.Request) {
	var agreement model.Agreement

	a.create(w, r,
		func() error {
			return decodeBody(w, r.Body, &agreement) //json.NewDecoder(r.Body).Decode(&agreement)
		},
		func() (model.Identity, error) {
			return a.Repository.CreateAgreement(&agreement)
		})
}

/*
DeleteAgreement deletes an agreement by id
*/
func (a *App) DeleteAgreement(w http.ResponseWriter, r *http.Request) {
	a.update(w, r, func(id string) error {
		return a.Repository.DeleteAgreement(&model.Agreement{Id: id})
	})
}

/* 
UpdateAgreement updates the only fields updateable by REST in an agreement: state and assessment.monitoringURL.
The Id in the body is ignored; only the id path is taken into account.
*/
func (a *App) UpdateAgreement(w http.ResponseWriter, r *http.Request) {
	var input model.Agreement

	a.updateEntity(w, r,
		func() error {
			return json.NewDecoder(r.Body).Decode(&input)
		},
		func(id string) (model.Identity, error) {
			newState := input.State
			ag, err := a.Repository.UpdateAgreementState(id, newState)
			if err != nil {
				return ag, err
			}
			modified := false
			if input.Assessment.MonitoringURL != "" {
				ag.Assessment.MonitoringURL = input.Assessment.MonitoringURL
				modified = true
			}
			if modified {
				ag, err = a.Repository.UpdateAgreement(ag)
			}
			return ag, err
		})
}

// StartAgreement starts monitoring an agreement
func (a *App) StartAgreement(w http.ResponseWriter, r *http.Request) {
	a.update(w, r, func(id string) error {
		_, err := a.Repository.UpdateAgreementState(id, model.STARTED)
		return err
	})
}

// StopAgreement stop monitoring an agreement
func (a *App) StopAgreement(w http.ResponseWriter, r *http.Request) {
	a.update(w, r, func(id string) error {
		_, err := a.Repository.UpdateAgreementState(id, model.STOPPED)
		return err
	})
}

// TerminateAgreement terminates an agreement
func (a *App) TerminateAgreement(w http.ResponseWriter, r *http.Request) {
	a.update(w, r, func(id string) error {
		_, err := a.Repository.UpdateAgreementState(id, model.TERMINATED)
		return err
	})
}

/* 
GetTemplates return all templates in db
*/
func (a *App) GetTemplates(w http.ResponseWriter, r *http.Request) {

	a.getAll(w, r, func() (interface{}, error) {
		return a.Repository.GetAllTemplates()
	})
}

/* 
GetTemplate gets a template by REST ID
*/
func (a *App) GetTemplate(w http.ResponseWriter, r *http.Request) {
	a.get(w, r, func(id string) (interface{}, error) {
		return a.Repository.GetTemplate(id)
	})
}

/* 
CreateTemplate creates a template passed by REST params
*/
func (a *App) CreateTemplate(w http.ResponseWriter, r *http.Request) {

	var template model.Template

	a.create(w, r,
		func() error {
			return json.NewDecoder(r.Body).Decode(&template)
		},
		func() (model.Identity, error) {
			return a.Repository.CreateTemplate(&template)
		})
}

/*
CreateAgreementFromTemplate generates an agreement from a template and parameters
*/
func (a *App) CreateAgreementFromTemplate(w http.ResponseWriter, r *http.Request) {

	var in model.CreateAgreement
	var t *model.Template
	var ag *model.Agreement

	a.create(w, r,
		func() error {
			return json.NewDecoder(r.Body).Decode(&in)
		},
		func() (model.Identity, error) {
			var err error

			t, err = a.Repository.GetTemplate(in.TemplateID)
			if err != nil {
				return nil, err
			}

			genmodel := generator.Model{
				Template:  *t,
				Variables: in.Parameters,
			}

			ag, err = generator.Do(&genmodel, a.validator, a.externalIDs)
			if err != nil {
				return nil, err
			}

			ag, err = a.Repository.CreateAgreement(ag)
			if err != nil {
				return nil, err
			}

			out := in
			out.AgreementID = ag.Id

			return &out, nil
		})
}

/* 
ReceiveNotification is an endpoint to test the sending of notifications to external endpoints
*/
func (a *App) ReceiveNotification(w http.ResponseWriter, r *http.Request) {

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		manageError(err, w)
		return
	}
	log.Infof("Received notification: %s", buf)
}

/* 
GetViolation gets violations from an SLA
*/
func (a *App) GetViolations(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.get(w, r, func(id string) (interface{}, error) {
		return a.Repository.GetViolations(id) //.GetAgreement(id)
	})
}

/* 
GetViolation gets violations from an application
*/
func (a *App) GetAppViolations(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	a.get(w, r, func(id string) (interface{}, error) {
		return a.Repository.GetAppViolations(id) //.GetAgreement(id)
	})
}

/* 
GetAllViolations gets all violations
*/
func (a *App) GetAllViolations(w http.ResponseWriter, r *http.Request) {
	a.getAll(w, r, func() (interface{}, error) {
		return a.Repository.GetAllViolations()
	})
}

// manageError
func manageError(err error, w http.ResponseWriter) {
	switch err {
	case model.ErrAlreadyExist:
		respondWithError(w, http.StatusConflict, "Object already exist")
	case model.ErrNotFound:
		respondWithError(w, http.StatusNotFound, "Can't find object")
	default:
		if model.IsErrValidation(err) || generator.IsErrUnreplaced(err) {
			respondWithError(w, http.StatusBadRequest, err.Error())
		} else {
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
	}
}

// respondWithError
func respondWithError(w http.ResponseWriter, code int, message string) {
	log.Error(pathLOG + "[REST API > respondWithError] Error: ", message)

	respondWithJSON(w, code, ApiError{strconv.Itoa(code), message})
}

// respondWithJSON
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	log.Info(pathLOG + "[REST API > respondWithJSON] Sending response with code " + strconv.Itoa(code))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	enc.Encode(payload)

}

// respondSuccessJSON
func respondSuccessJSON(w http.ResponseWriter, payload interface{}) {
	respondWithJSON(w, http.StatusOK, payload)
}

// respondNoContent
func respondNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

// decodeBody function, used to get more information in case of errors
func decodeBody(w http.ResponseWriter, body io.ReadCloser, obj interface{}) error {
	// Use http.MaxBytesReader to enforce a maximum read of 1MB from the
	// response body. A request body larger than that will now result in
	// Decode() returning a "http: request body too large" error.
	body = http.MaxBytesReader(w, body, 1048576)

	// Setup the decoder and call the DisallowUnknownFields() method on it.
	// This will cause Decode() to return a "json: unknown field ..." error
	// if it encounters any extra unexpected fields in the JSON. Strictly
	// speaking, it returns an error for "keys which do not match any
	// non-ignored, exported fields in the destination".
	dec := json.NewDecoder(body)
	//dec.DisallowUnknownFields()

	// decode
	err := dec.Decode(&obj)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
			// Catch any syntax errors in the JSON and send an error message
			// which interpolates the location of the problem to make it
			// easier for the client to fix.
			case errors.As(err, &syntaxError):
				msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
				log.Error("Error decoding input (1): ", msg)
	
			// In some circumstances Decode() may also return an
			// io.ErrUnexpectedEOF error for syntax errors in the JSON. There
			// is an open issue regarding this at
			// https://github.com/golang/go/issues/25956.
			case errors.Is(err, io.ErrUnexpectedEOF):
				msg := fmt.Sprintf("Request body contains badly-formed JSON")
				log.Error("Error decoding input (2): ", msg)
	
			// Catch any type errors, like trying to assign a string in the
			// JSON request body to a int field in our Person struct. We can
			// interpolate the relevant field name and position into the error
			// message to make it easier for the client to fix.
			case errors.As(err, &unmarshalTypeError):
				msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
				log.Error("Error decoding input (3): ", msg)
	
			// Catch the error caused by extra unexpected fields in the request
			// body. We extract the field name from the error message and
			// interpolate it in our custom error message. There is an open
			// issue at https://github.com/golang/go/issues/29035 regarding
			// turning this into a sentinel error.
			case strings.HasPrefix(err.Error(), "json: unknown field "):
				fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
				msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
				log.Error("Error decoding input (4): ", msg)
	
			// An io.EOF error is returned by Decode() if the request body is
			// empty.
			case errors.Is(err, io.EOF):
				msg := "Request body must not be empty"
				log.Error("Error decoding input (5): ", msg)
	
			// Catch the error caused by the request body being too large. Again
			// there is an open issue regarding turning this into a sentinel
			// error at https://github.com/golang/go/issues/30715.
			case err.Error() == "http: request body too large":
				msg := "Request body must not be larger than 1MB"
				log.Error("Error decoding input (6): ", msg)
	
			// Otherwise default to logging the error and sending a 500 Internal
			// Server Error response.
			default:
				log.Error("Error decoding input (7): ", err.Error())
				//http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	}

	return err
}