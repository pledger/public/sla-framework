# SLALite #

&copy; Atos Spain S.A. 2017

[![License: Apache v2](https://img.shields.io/badge/License-Apache%20v2-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

## Description ##

The [PLEDGER](http://www.pledger-project.eu/) SLA Framework tool is a lightweight implementation of an SLA system, inspired by the WS-Agreement standard. Its features are:

* REST interface to manage the creation and update of agreements
* Agreements evaluation on background; any breach in the agreement terms generates an SLA violation.
* Configurable monitoring: a monitoring has to be provided externally. In the case of Pledger, this tool connects to the PLEDGER Monitoring Engine component to get the metrics values.
* Configurable repository: a memory repository (for developing purposes) and a MongoDB repository are provided, but more can be added.
* Management of historical data about monitored applications and infrastructures. This includes information about metrics and SLA evaluations and also violations.
* UI Application made in React JS.

It is an evolution of the ATOS SLALite application / asset used in other projects and environments. It is composed by the following components / applications:

* SLALite application 
* Swagger REST API
* SLALite UI
* MongoDB

### SLAs ###

An agreement (SLA) is represented by a simple JSON structure (see more examples in resources/samples):

```json
{
    "id": "a01",
    "name": "an-agreement-name",
    "state": "started",
    "service": "<ID_APPLICATION>",
    "details": {
      "id": "a01",
      "type": "agreement",
      "name": "an-agreement-name",
      "creation": "2021-01-16T17:09:45Z",
      "provider": {
        "id": "id",
        "name": "name"
      },
      "client": {
        "id": "id",
        "name": "name",
        "warning": "warning"
      },
      "guarantees": [
        {
          "name": "TestGuarantee",
          "constraint": "execution_time < 100",
          "importance": [
            {
              "name": "Warning",
              "constraint": "> 100"
            },
            {
                "name": "Mild",
                "constraint": "> 150"
            },
            {
                "name": "Severe",
                "constraint": "> 200"
            },
            {
                "name": "Critical",
                "constraint": "> 400"
            }
          ],
          "penalties": [
            {
              "type": "type",
              "value": "value",
              "unit": "unit"
            }
          ],
          "actions": [
            {
              "type": "type",
              "value": "value",
              "unit": "unit"
            }
          ]
        }
      ]
    }
  }
```

It contains the **constraints**, **importance** (violation level), **penalties** and **actions**. In the case of Pledger 'actions' (what to do in case there is a violation) are not used as this is something handled by other Pledger core components.

----------------------------

## Usage guide ##

### Kubernetes / from Docker Hub repositories ###

#### Installation ####

Applications:

* SLALite application 
* Swagger REST API
* SLALite UI
* MongoDB

**SLALite** and **Swagger REST API** applications are containerized in the same image, located in _atospledger/sla-framework_. The **SLALite UI** can be found in _atospledger/sla-framework-ui_. And finally, _MongoDB_ application comes in an other dockerized image (in the deployment YAML file is defined the location of the MongoDB application).

Images can be found on https://hub.docker.com/u/atospledger.

```
docker pull atospledger/sla-framework

docker pull atospledger/sla-framework-ui
```

##### deployment YAML #####

In folder '_kustomizer_' you can find the deployment YAML files used to deploy the application in different Kubernetes environments. This includes the **MongoDB**, the **SLA UI** and the **SLA framework** applications deployment.

File '_kustomizer/base/slalite-app.yaml_' contains a list of environment variables that need to be set in the case of Pledger context. These are the variables needed to connect to Kafka and other core components of the Pledger solution.

----------------------------

### Docker / from source code ###

#### Installation ####

Build the Docker image:

    make docker

Run the container:

    docker run -ti -p 8090:8090 slalite:<version>

Stop execution pressing CTRL-C

To run the service under HTTPs, you must change supply a different configuration file and the certificate files. You will find these files in docker/https for debugging purposes.

    docker run -ti -p 8090:8090 -v $PWD/docker/https:/etc/slalite slalite

#### Configuration ####

The SLALite can be configured with a configuration file and with environment variables. The configuration file is read by default from /etc/slalite and the current working directory. The `-f` parameter can be used to set the config file location.

```
$ ./SLALite -h
Usage of SLALite:
  -b string
        Filename (w/o extension) of config file (default "slalite")
  -d string
        Directories where to search config files (default "/etc/slalite:.")
  -f string
        Path of configuration file. Overrides -b and -d
```

##### File settings #####

_main.go_ may need to be edited to use the following settings:

*General settings*

* `singlefile` (default: `false`). Sets if all file settings are read   from a single file or from several files. For example, when `singlefile=false`,  the MongoDB settings are read from the file `mongodb.yml`.
* `repository` (default: `memory`). Sets the repository type to use. Set this  value to `mongodb` to use a MongoDB database.
* `externalIDs` (default: `false`). Set this to true if the repository auto assign   the IDs of the saved entities.
* `checkPeriod` (default: `60s`). Sets the period of assessments executions, in the  format of a time.Duration (e.g. 60s, 1.5m). If no unit is given, seconds are assumed.
* `transientTime` (default: `0s`). Sets the transient time after a violation on a   guarantee term is raised, in the format of a time.Duration (e.g. 60s, 1.5m). No more   violations on that term will be raised while in the transient time.   If no unit is given, seconds are assumed.
* `CAPath`. Sets the value of a file path containing certificates of trusted  CAs; to be used to connect as client to SSL servers whose certificate is  not trusted by default (e.g. self-signed certificates)

*REST interface settings*

* `port` (default: `8090`). Port of REST interface.
* `enableSsl` (default: `false`). Enables the use of SSL on the REST  interface. The two following variables should be set.
* `sslCertPath` (default: `cert.pem`). Sets the certificate path.
* `sslKeyPath` (default: `key.pem`). Sets the private key path to access the  certificate.

*MongoDB settings (default file: /etc/slalite/mongodb.yml)*

* `connection` (default: `localhost`). Sets the MongoDB host.
* `database` (default: `slalite`). Sets the MongoDB database name to use.
* `clear_on_boot` (default: `false`). Sets if the database is cleared on  startup (useful for tests).

*Blockchain notifier*

* `notificationUrl` (default: none). If set, enables the notification to the  REB. The violations are notified to the REST endpoint defined by 
  `notificationURL/notificationPath` properties.
* `notificationPath` (default: `decenter/decenter-resource-sharing-api/1.0.1`). 
  See `notificationUrl` property.
##### Env vars  #####

Every file setting can be overriden with the use of environment variables. The name of the var is the uppercase setting name prefixed with `SLA_`. For example, to override the check period, set the env var `SLA_CHECKPERIOD`.

#### Usage ####

SLALite offers a usual REST API, with an endpoint on /agreements

Add an agreement (agreement below is stopped):

    curl -k -X POST -d @resources/samples/agreement.json http://localhost:8090/agreements

Change agreement state:

    curl -k http://localhost:8090/agreements/a02 -X PATCH -d'{"state":"started"}'

Get agreements:

    curl -k http://localhost:8090/agreements
    curl -k http://localhost:8090/agreements/a02

Add a template:

    curl -k -X POST -d @resources/samples/template.json http://localhost:8090/templates

Get templates:

    curl -k http://localhost:8090/templates
    curl -k http://localhost:8090/templates/t01

Create agreement from template:

    curl -k -X POST -d @resources/samples/create-agreement.json http://localhost:8090/create-agreement

    {"template_id":"t01","agreement_id":"9be511e8-347f-4a40-b784-e80789e4c65b","parameters":{"M":1,"N":100,"agreementname":"An agreement name","client":{"id":"client01","name":"A name of a client"},"provider":{"id":"provider01","name":"A name of a provider"}}}

## LICENSES
SLA Framework component is licensed under [Apache License, version 2](License.txt).

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|