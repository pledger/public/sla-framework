##########################################
FROM golang:alpine as builder

ARG BUILD_ID
LABEL stage=builder
LABEL build=$BUILD_ID

RUN apk add --no-cache git curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

WORKDIR /go/src/SLALite

COPY . .

RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -o SLALite .

##########################################
FROM alpine:3.6

WORKDIR /opt/slalite

COPY --from=builder /go/src/SLALite/SLALite .
COPY --from=builder /go/src/SLALite/resources /opt/slalite/resources
COPY ./swaggerui ./swaggerui

RUN mkdir /etc/slalite
RUN addgroup -S slalite && adduser -D -G slalite slalite
RUN chown -R slalite:slalite /etc/slalite && chmod 700 /etc/slalite

EXPOSE 8090

USER slalite

ENTRYPOINT ["/opt/slalite/SLALite"]
