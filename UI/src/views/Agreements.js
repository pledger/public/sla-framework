//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Button} from 'react-bootstrap';


// SLA Manager REST API
const slamngr = require('./slamngr')
var api = slamngr.RESTAPI("");

// Empty agreement
var EMPTY_AGREEMENT = {
  id: "DEFAULT",
  details: {
    name: "",
    client: {
      name: "",
    },
    provider: {
      name: "",
    },
    guarantees: [],
    assessment: {
      first_execution: "",
      last_execution: "",
      guarantees: []}
  }
}

// Empty assessment
var EMPTY_ASSESSMENT = {
  first_execution: "",
  last_execution: "",
  guarantees: []
}

/*
get agreements
*/
async function getAgreements() {
  return api.get('agreements')
}


///////////////////////////////////////////////////////////////////////////////
// AGREEMENTS
///////////////////////////////////////////////////////////////////////////////
class Agreements extends Component {
  /*
  Constructor: state initialization, bindings
  */
  constructor(props, context) {
    super(props, context);

    console.log("loading data ...");

    // state
    this.state = {
      agreements: [],
      agreement: EMPTY_AGREEMENT,
      violations: []
    }

    // binding
    this.handleClick = this.handleClick.bind(this);
    this.loadSLAs = this.loadSLAs.bind(this);
  }

  /*
  componentDidMount: after component is mount tasks
  */
  async componentDidMount() {
    var a = await getAgreements();
    this.setState( (state, props) => {
      return {
        instances : a,
        agreement: EMPTY_AGREEMENT,
        violations: []
      }
    })

    console.log("loading SLAs ...");
    this.loadSLAs(null);
  }

  /*
   Load / Reload SLA Instances
  */
  async loadSLAs(e) {
    var that = this;
    const data = await getAgreements()
    that.setState( {agreements: data});
  }

  /*
   Select SLA
  */
  handleClick(e) {
    const id = e.target.value;
    this.setState({
      agreement: this.getAgreement(id)
    });
  }

  getAgreement(id) {
    for (var i = 0; i < this.state.agreements.length; i++) {
      if (this.state.agreements[i].id == id) {
        console.log(this.state.agreements[i]);
        return this.state.agreements[i];
      }
    }
    return EMPTY_AGREEMENT;
  }

  formatDate(s) {
    return s;
  }

  formatValue(values) {
    return JSON.stringify(values);
  }

  /*
  render function

  {this.state.agreements.assessment.map(item => (
    <tr key={item.id}>
      <td>{this.formatDate(item.datetime)}</td>
      <td>{item.guarantee}</td>
      <td>{this.formatValue(item.values)}</td>
    </tr>
  ))}
  */
  render() {
    let visible = this.state.agreement.id !== EMPTY_AGREEMENT.id;

    let assessment = EMPTY_ASSESSMENT;
    if (this.state.agreement.assessment != null) {
      assessment = {
        first_execution: this.state.agreement.assessment.first_execution,
        last_execution: this.state.agreement.assessment.last_execution,
        guarantees: []
      }
    }

    let assessment_guarantees = {};
    if ((this.state.agreement.assessment != null) && (this.state.agreement.assessment.guarantees != null)) {
      assessment_guarantees = this.state.agreement.assessment.guarantees
    } else {
      assessment_guarantees = {}
    }

    return (
      <div className="agreements" style={{margin: "0px 15px 0px 15px"}}>
        <div class="text-right">
          <Button variant="outline-dark" onClick={this.loadSLAs} size="sm">
            <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Reload SLA Instances</Button>
          &nbsp;&nbsp;
        </div>

        <div>
          <SLAInstancesTable value={this.state} onClick={(e) => this.handleClick(e)} />
        </div>

        <hr />

        <div style={{margin: "0px 0px 0px 35px"}}>
          <div style={{display: visible? 'block': 'none'}}>
            <h5>Agreement: {this.state.agreement.id}</h5>
            <table className="table table-sm table-dark">
              <thead>
                <tr>
                  <th>Client</th>
                  <th>Provider</th>
                </tr>
              </thead>
              <tbody class="table-primary">
                <tr>
                  <td><font color="black"><i>{this.state.agreement.details.client.name}</i></font></td>
                  <td><font color="black"><i>{this.state.agreement.details.provider.name}</i></font></td>
                </tr>
              </tbody>
            </table>

            <h5>Guarantees</h5>
            <table className="table table-sm table-hover table-dark">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Constraint</th>
                </tr>
              </thead>
              <tbody class="table-info">
                {this.state.agreement.details.guarantees.map(item => (
                  <tr key={item.name}>
                    <td><font color="black">{item.name}</font></td>
                    <td><font color="black">{item.constraint}</font></td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

        <div style={{margin: "0px 0px 0px 35px"}}>
          <div style={{display: visible? 'block': 'none'}}>
            <h5>Assessment</h5>
            <table className="table table-sm table-hover table-dark">
              <thead>
                <tr>
                  <th>first execution</th>
                  <th>last execution</th>
                </tr>
              </thead>
              <tbody class="table-info">
                <tr>
                  <td><font color="black">{assessment.first_execution}</font></td>
                  <td><font color="black">{assessment.last_execution}</font></td>
                </tr>
              </tbody>
            </table>
            <h6>&nbsp;&nbsp;<b>Violations:</b></h6>
            <table className="table table-sm table-hover">
              <tbody class="table-danger">
                {Object.entries(assessment_guarantees).map(([k, v]) => (
                  v.last_violation != null?
                    <tr key={k}>
                      <td><font color="darkred"><b>{k}</b></font></td>
                      <td><font color="black">{v.last_violation.datetime}</font></td>
                      <td><font color="black">{v.last_violation.guarantee}</font></td>
                      <td><font color="black">{v.last_violation.constraint}</font></td>
                    </tr>: <tr>&nbsp;</tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

        {(this.state.agreement != null) && (this.state.agreement.id != "DEFAULT")?
          <div style={{margin: "0px 0px 0px 35px"}}>
            <h5>SLA (full JSON)</h5>
            <div className="form-group row">
              <div className="col-sm-12">
                <textarea className="form-control" id="job" rows="50"
                  value={JSON.stringify(this.state.agreement, null, 2)}>
                </textarea>
              </div>
            </div>
          </div>: null}

      </div>
    );
  }
}


///////////////////////////////////////////////////////////////////////////////
// SLAInstancesTable
///////////////////////////////////////////////////////////////////////////////
class SLAInstancesTable extends React.Component {
  /*
  Constructor: state initialization, bindings
  */
  constructor(props) {
    super(props);
    this.state = {
      agreements: [],
      agreement: {}
    }
  }

  /*
  render function

  <th>#Violations</th>
  <td>{ this.props.value.nViolations[item.id] }</td>


  // this.props.value.agreements
  */
  render() {
    let agreements = this.props.value.agreements;

    if (agreements == null) {
      agreements = [];
    }

    return(
      <div style={{margin: "-20px 0px 0px 0px"}}>
        <h3><font color="#2B547E">SLA Instances</font></h3>
        <table className="table table-sm table-hover table-striped">
          <thead class="thead-dark">
            <tr>
              <th>#</th>
              <th>Client</th>
              <th>SLA Name</th>
              <th>State</th>
              <th>Creation</th>
            </tr>
          </thead>
          <tbody>
            {agreements.map((item,index) => (
              <tr key={item.id} className={this.isViolated(item)? 'table-danger' : ''}>
                <td>
                  <button value={item.id} onClick={ (e) => this.props.onClick(e) } className="btn btn-link" style={{margin: "-5px 0px 0px 0px"}}>
                    <font color="black">{ index }</font>
                  </button>
                </td>
                <td>{item.details.client.name}</td>
                <td>{item.name}</td>
                <td>{item.state == "started" ? (
                  <font color="lightgreen">started</font>
                ) : (
                  <font color="black">{item.state}</font>
                )}</td>
                <td>{item.details.creation}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }

  getType(si) {
    return si.service_type;
  }

  isViolated(item) {
    if ((item.assessment != null) && (item.assessment.guarantees != null)) {
      for (const property in item.assessment.guarantees) {
        if ((item.assessment.guarantees[property].last_violation != null) && (item.assessment.guarantees[property].last_violation.datetime != null)) {
          return true;
        }
      }
    }
    return false;
  }
}


// export
export default Agreements;
