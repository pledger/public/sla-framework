//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Button, Card, CardColumns, InputGroup, FormControl } from 'react-bootstrap';


/**
 * Config
 */
class Config extends Component {

  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      url_rest_api_lm: global.rest_api_lm,
      orchestrators: [],
      apps: []
    }

    // functions binding
    this.handleChange_rest_api_lm = this.handleChange_rest_api_lm.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
  }


  handleChange_rest_api_lm(event) {
    this.setState({url_rest_api_lm: event.target.value});
  }


  handleSave(event) {
    global.rest_api_lm = this.state.url_rest_api_lm;
  }


  handleRestore(event) {
    global.rest_api_lm = global.const_rest_api_lm;

    this.setState({url_rest_api_lm: global.rest_api_lm});
  }


  render() {
    return (
      <div style={{margin: "35px 0px 0px 0px"}}>
        <CardColumns>

          <Card bg="secondary" text="white" style={{ width: '21rem' }}>
            <Card.Header><b>SLA Manager REST API</b></Card.Header>
            <Card.Body>
              <Card.Text>
                <InputGroup size="sm" className="mb-3">
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" value={this.state.url_rest_api_lm}
                    onChange={this.handleChange_rest_api_lm} style={{ backgroundColor: "#E0F2F7" }}/>
                </InputGroup>
              </Card.Text>

              <Button variant="info" size="sm" onClick={this.handleSave}><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</Button>
              &nbsp;
              <Button variant="light" size="sm" onClick={this.handleRestore}><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;Restore default</Button>
            </Card.Body>
          </Card>

        </CardColumns>

      </div>
    );
  }
}

export default Config;
