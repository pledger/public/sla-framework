//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
const axios = require('axios');
const https = require('https')

const defaultURL = 'http://localhost:8090/'

var RESTAPI = (baseurl, auth) => {

    // TODO: pass auth if not undefined to headers
    const baseURL = baseurl || defaultURL;
    const slaRestAPI = axios.create({
        baseURL: baseURL,
        httpsAgent: new https.Agent({
            rejectUnauthorized: false
        })
    });

    async function cloudEntryPoint() {
        let response = await slaRestAPI.get('cloud-entry-point');
        return response.data;
    }

    async function violations(query = "") {
        let data = await get(path('sla-violation', query));
        return data.slaViolations;
    }

    async function agreements(query = "") {
        let data = await get(path('agreements', query));
        return data.agreements;
    }

    async function get(path) {
        var urlPath = ""
        if (global.rest_api_lm.endsWith("/")) {
            urlPath = global.rest_api_lm + path;
        } else {
            urlPath = global.rest_api_lm + "/" + path;
        }
        let response = await slaRestAPI.get (urlPath) //(`${path}`);
        if (response.status >= 400) {
            let request = response.request;
            throw new Error(`${response.status} ${response.statusText}: ${request.method} ${request.path}`)
        }
        return response.data;
    }

    async function post(path, entity) {
        let response = await slaRestAPI.post (global.rest_api_lm + path, entity) //(path, entity);
        if (response.status >= 400) {
            let request = response.request;
            throw new Error(`${response.status} ${response.statusText}: ${request.method} ${request.path}`)
        }
        return response.data;
    }

    function path(name, query = "") {
        return `${name}?${query}`;
    }

    return {
        baseURL,
        get,
        post,
        cloudEntryPoint,
        violations,
        agreements,
        slaRestAPI,
    }
}

export {
    RESTAPI
}
