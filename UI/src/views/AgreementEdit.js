//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 19 Mar 2021
// Updated on 19 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import request from "request";
import { Alert, Spinner, Button, Tabs, TabPane, OverlayTrigger, Tooltip, Badge, Form, Row, Col, Dropdown } from 'react-bootstrap';
import { useHistory } from "react-router-dom";


/**
 * AppEdit
 */
class AppEdit extends Component {


  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
      serv_def: "{}",
      total_apps_1: 0,
      sel_service_instance_id_1: "",
      job_def: "",
      start_si_button: false,
      report_si_button: false,
      cancel_si_button: false,
      msg: "",
      msg_content: "",
      show_alert: false,
      show_info: false,
      msg2: "",
      msg_content2: "",
      show_alert2: false,
      show_info2: false,
      job_report: "",

      notCreatingNew: true,

      f: {
        replicas: 1
      },
      apps: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
    this.handleChange_serv_def = this.handleChange_serv_def.bind(this);
  }


  routeChange=()=> {
    let path = `apps`;
    let history = useHistory();
    history.push(path);
  }


  // 
  handleChange_serv_def(event) {
    this.setState({serv_def: event.target.value});
  }


  // Launch application from JSON tab
  handleSubmit(event) {
    this.setState({isLoading: true});
    console.log("Creating a new SLA (json tab) ...");

    // call to api
    try {
      var that = this;
      var formData = JSON.parse(this.state.serv_def);
      console.log(formData);
      console.log(global.rest_api_lm + "apps");

      request.post({url: global.rest_api_lm + "apps", json: formData}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "POST /api/v1/apps", msg_content: err.toString() });
        }
        else {
          console.log("Launching a new application in Pledger platform ... ok");
          if ((resp.statusCode == 200) && (body["resp"] != null) && (body["resp"] == "error")) {
            that.setState({ show_alert: true, msg: "Error deploying application. Response: ", msg_content: JSON.stringify(body) });
          }
          else if (resp.statusCode == 200) {
            that.setState({ show_info: true, msg: "Application deployed. Response: ", msg_content: JSON.stringify(body) });
          }
          else {
            that.setState({ show_alert: true, msg: "POST /api/v1/apps => " + resp.statusCode + " / Undefined response obtained: ", msg_content: JSON.stringify(body) });
          }

          that.setState({ notCreatingNew: true});
        }

        that.setState({isLoading: false});
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "POST /api/v1/apps", msg_content: err.toString(), isLoading: false });
    }
  }

  // 
  handleChange_serv_def(event) {
    this.setState({serv_def: event.target.value});
  }

  
  /**
   * Render elements
   */
  render() {
    return (
      <div style={{margin: "0px 0px 0px 0px"}}>
        {this.state.isLoading ?
          <Spinner animation="border" role="status" variant="primary">
            <span className="sr-only">Loading...</span>
          </Spinner> : ""}

        <Alert variant="danger" toggle={this.onDismiss} show={this.state.show_alert}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
                <Button onClick={() => this.setState({ show_alert: false })} variant="outline-danger">
                Close
                </Button>
            </div>
            </Alert>

            <Alert variant="primary" toggle={this.onDismiss} show={this.state.show_info}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
                <Button onClick={() => this.setState({ show_info: false })} variant="outline-primary">
                Close
                </Button>
            </div>
        </Alert>

        <form>
          <div className="form-group row">
            <div className="col-sm-7">
              <i class="fa fa-cogs" aria-hidden="true"></i>
              &nbsp;
              <b>Applications</b> Create and launch a new application
            </div>
            <div className="col-sm5">
            </div>
          </div>

          <div className="form-group row" style={{margin: "-20px 0px 0px 0px"}}>
            <div className="col-sm-12" style={{margin: "10px 0px 0px 0px"}}>

              <Tabs defaultActiveKey="json">
                <TabPane eventKey="json" title="json" style={{backgroundColor: "white"}}>
                  <p><br />&nbsp;&nbsp;<i>SLA creation</i></p>

                  <form onSubmit={this.handleSubmit}  style={{margin: "10px 10px 10px 10px"}}>
                    <div className="form-group row">
                      <div className="col-sm-12">
                        <textarea className="form-control" id="job" rows="20" value={this.state.serv_def} onChange={this.handleChange_serv_def}>
                        </textarea>
                      </div>
                    </div>
                  </form>

                  <div className="col-sm-12">
                    &nbsp;
                    <Button variant="outline-success" onClick={this.handleSubmit} disabled={this.state.isLoading} size="sm"><i class="fa fa-rocket" aria-hidden="true"></i>&nbsp;Deploy</Button>
                    &nbsp;
                    <Button variant="outline-danger" onClick={this.cancel} disabled={this.state.isLoading} size="sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel</Button>
                  </div>
                  < br/>
                </TabPane>

              </Tabs>
            </div>
          </div>
        </form>
      </div>
    );
  }

}

export default AppEdit;