//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import { Card, CardColumns, ListGroup } from 'react-bootstrap';


class Home extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div style={{margin: "15px 5px 5px 5px"}}>
        <CardColumns style={{display: 'flex', flexDirection: 'row'}}>

          <Card bg="light" style={{flex: 1, color: '#fff', background: 'url(img/bg22.jpg) 100% no-repeat'}}>
            <Card.Img variant="top" src="img/logo122.png"  style={{width: "195px"}} />
            <Card.Body>
              <Card.Text>
              The Pledger project aims at delivering a new architectural paradigm and a toolset that will pave the way for next generation edge computing infrastructures, tackling the modern challenges faced today and coupling the benefits of low latencies on the edge, with the robustness and resilience of cloud infrastructures.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card bg="light" style={{flex: 1, color: '#000', background: 'url(img/bg_sla_2.png) 100% no-repeat'}}>
            <Card.Body>
              <Card.Title><b>SLA Management</b></Card.Title>
              <Card.Subtitle className="mb-2 text-muted" class="text-justify">
                <p>The SLALite is a lightweight implementation of an SLA system, inspired by the WS-Agreement standard. Its features are:</p>
                <p>&nbsp;REST interface to manage creation and update of agreements</p>
                <p>&nbsp;Agreements evaluation on background; any breach in the agreement terms generates an SLA violation.</p>
                <p>&nbsp;Configurable monitoring: a monitoring has to be provided externally.</p>
                <p>&nbsp;Configurable repository: a memory repository (for developing purposes) and a mongodb repository are provided, but more can be added.</p>
              </Card.Subtitle>
            </Card.Body>
          </Card>

          <Card bg="light" text="black" style={{flex: 1}}>
            <Card.Img variant="top" src="img/howitworks.png" fluid/>
            <Card.Body>
              <Card.Title><b>SLA-Framework architecture</b></Card.Title>
            </Card.Body>
          </Card>

        </CardColumns>
      </div>
    );

  }
}

export default Home;
