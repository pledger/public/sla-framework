//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React from 'react';
import { useIntl } from 'react-intl';
import {
  HashRouter,
  Link
} from 'react-router-dom';
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';
import { FaTachometerAlt, FaCopy, FaWrench } from 'react-icons/fa';
import sidebarBg from './assets/bg21.jpg';


/**
 * Aside
 */
const Aside = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  const intl = useIntl();
  return (
    <HashRouter>
      <ProSidebar
        image={image ? sidebarBg : false}
        rtl={rtl}
        collapsed={collapsed}
        toggled={toggled}
        breakPoint="md"
        onToggle={handleToggleSidebar}
        collapseOnSelect
      >
        <SidebarHeader>
          <div
            style={{
              padding: '20px',
              fontWeight: 'bold',
              fontSize: 14,
              letterSpacing: '1px',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
            }}
          >

            <font color="white" size="2"><b>SLA Framework UI</b></font>&nbsp;
            <font color="lightgray" size="1"><i>v</i></font><font color="lightgray" size="2"><i>0.1.4</i></font>
          </div>
        </SidebarHeader>

        <SidebarContent style={{
          fontWeight: 'bold',
          fontSize: 14,
          color: "white",
          height: "100%"
        }}>
          <Menu iconShape="circle">
            <MenuItem icon={<FaTachometerAlt />}>
              {intl.formatMessage({ id: 'dashboard' })}
              <Link to="home" />
            </MenuItem>

            <MenuItem icon={<FaCopy />}>
              {intl.formatMessage({ id: 'Agreements' })}
              <Link to="agreements" />
            </MenuItem>

            <SubMenu title={intl.formatMessage({ id: 'Configuration' })} icon={<FaWrench />}>
              <MenuItem>{intl.formatMessage({ id: 'Logs' })}</MenuItem>
              <MenuItem>{intl.formatMessage({ id: 'Setup' })}
                <Link to="config" />
              </MenuItem>
            </SubMenu>
          </Menu>
        </SidebarContent>

        <SidebarFooter style={{ textAlign: 'center' }}>
          <div
            className="sidebar-btn-wrapper"
            style={{
              padding: '20px 24px',
            }}
          >

            <small>©2020 Copyright</small>
                <a href="https://atos.net/en/" target="_blank">
                  <font color="darkgray" size="2">&nbsp;<b>ATOS</b></font>&nbsp;&nbsp;&nbsp;</a>
              
          </div>
        </SidebarFooter>
      </ProSidebar>
    </HashRouter>
  );
};

export default Aside;
