//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 24 Mar 2021
//
// @author: ATOS
//
import React from 'react';
import { Navbar, Nav, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useIntl } from 'react-intl';
import Switch from 'react-switch';
import Home from "./views/Home";
import Config from "./views/Config";
import Agreements from "./views/Agreements";
import {
  Route,
  HashRouter
} from "react-router-dom";


/*
* Main
*/
const Main = ({
  collapsed,
  image,
  handleCollapsedChange,
  handleImageChange,
}) => {
  const intl = useIntl();

  var swagger = "http://localhost:8090/swaggerui/";
  if (global.rest_api_lm != null) {
    //swagger = global.rest_api_lm.replace("api/v1", "swaggerui");
    swagger = global.rest_api_lm + "/swaggerui/";
  }

  return (
    <div className="container-fluid">
      <Navbar bg="dark" variant="dark" expand="lg" style={{margin: "0px -10px 10px -10px"}}>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto" as="ul">
            <Nav.Item as="li">
              <Switch
                height={16}
                width={30}
                checkedIcon={false}
                uncheckedIcon={false}
                onChange={handleCollapsedChange}
                checked={collapsed}
                onColor="#219de9"
                offColor="#bbbbbb"
              />
            </Nav.Item>

            <Nav.Item as="li" hidden>
              <Switch
                height={16}
                width={30}
                checkedIcon={false}
                uncheckedIcon={false}
                onChange={handleImageChange}
                checked={image}
                onColor="#219de9"
                offColor="#bbbbbb"
              />
            </Nav.Item>

            <div class="vl"></div>

            <Nav className="mr-auto" defaultActiveKey="#/" as="ul" className="ml-auto">
              <Nav.Item as="li">
                <OverlayTrigger placement="bottom" delay={{ show: 250, hide: 400 }} overlay={<Tooltip>Swagger REST API</Tooltip>}>
                  <Nav.Link exact href={swagger} target="_blank">
                    <i class="fa fa-home" aria-hidden="true"></i>&nbsp;<b>Swagger</b>
                  </Nav.Link>
                </OverlayTrigger>
              </Nav.Item>
            </Nav>

            <Nav className="mr-auto" defaultActiveKey="#/" as="ul">
              <Nav.Item as="li">
                <OverlayTrigger placement="bottom" delay={{ show: 250, hide: 400 }} overlay={<Tooltip>REST API</Tooltip>}>
                  <Nav.Link exact href={global.rest_api_lm} target="_blank">
                    <i class="fa fa-home" aria-hidden="true"></i>&nbsp;<b>REST API</b>
                  </Nav.Link>
                </OverlayTrigger>
              </Nav.Item>
            </Nav>

          </Nav>
        </Navbar.Collapse>

        <Navbar.Brand href="http://www.pledger-project.eu/" target="_blank">
          <img
            src="img/logo122.png"
            width="140"
            height="35"
            className="d-inline-block align-top"
            alt="Pledger EU Project"
          />
        </Navbar.Brand>
      </Navbar>

      <div  style={{overflow: 'auto', height:'92%'}}>
        <HashRouter>
            <Route exact path="/" component={Home}/>
            <Route path="/home" component={Home}/>
            <Route path="/agreements" component={Agreements}/>
            <Route path="/config" component={Config}/>
        </HashRouter>
      </div>

    </div>
  );
};

export default Main;
