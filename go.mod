module atos.pledger/sla-framework

go 1.15

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/confluentinc/confluent-kafka-go v1.6.1 // indirect
	github.com/coreos/bbolt v1.3.2
	github.com/eclipse/paho.mqtt.golang v1.3.2
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/labstack/gommon v0.3.0
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/prometheus/client_golang v0.9.3
	github.com/segmentio/kafka-go v0.4.12
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
)
