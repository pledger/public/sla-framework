//
// Copyright 2021 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2021
// Updated on 21 April 2021
//
// @author: ATOS
//
// Package metrics contains custom application metrics.
package metrics

import (
	log "atos.pledger/sla-framework/common/logs"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	counter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "slalite",
			Name:      "total_violations",
			Help:      "Total violations",
		},
		[]string{"application", "agreement", "metric", "importance"},
	)

	gauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "slalite",
			Name:      "violations",
			Help:      "Agreement violations",
		},
		[]string{"application", "agreement", "metric", "importance"},
	)
)

/* 
Init initialize metrics HTTP interface
*/
func Init() {
	prometheus.MustRegister(counter)
	prometheus.MustRegister(gauge)
}

/* 
CountViolation Counter of violations
*/
func CountViolation(value float64, labels map[string]string) {
	counter.With(prometheus.Labels{
		"application": labels["application"],
		"agreement":   labels["agreement"],
		"metric":      labels["metric"],
		"importance":  labels["importance"],
	}).Inc()

	log.Trace("[METRICS] CountViolation %f {%s}\n", value, labels)
}

/* 
AddSample allow to add a new sample value to the time serie database
*/
func AddSample(value float64, labels map[string]string) {
	gauge.With(prometheus.Labels{
		"application": labels["application"],
		"agreement":   labels["agreement"],
		"metric":      labels["metric"],
		"importance":  labels["importance"],
	}).Set(value) //.Add(value)

	log.Trace("[METRICS] AddSample %f {%s}\n", value, labels)
}
