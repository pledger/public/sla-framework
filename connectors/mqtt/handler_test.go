//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package mqtt

import (
	"atos.pledger/sla-framework/connectors/reb"
	"atos.pledger/sla-framework/model"
	"atos.pledger/sla-framework/repositories/memrepository"
	"atos.pledger/sla-framework/repositories/validation"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"reflect"
	"sync"
	"testing"

	"github.com/spf13/viper"
)

const (
	reservationPath  = "../reb/testdata/reservation.json"
	reservation2Path = "../reb/testdata/reservation2.json"
)

func newHandler(tpl model.Template) AgreementCreator {
	config := viper.New()

	/*
	 * This initialization replicates the one in main.go
	 */
	val := model.NewDefaultValidator(false, true)
	var repo model.IRepository
	repo, _ = memrepository.New(config)
	repo, _ = validation.New(repo, val)

	handler := AgreementCreator{
		Repo:      repo,
		Template:  &tpl,
		Val:       val,
		rebclient: nil,
	}
	return handler
}

func TestNewHandler(t *testing.T) {
	config := viper.New()
	config.Set(TemplatePathPropertyName, "testdata/template.json")
	_, err := NewHandler(config, nil, nil, nil)
	if err != nil {
		t.Errorf("Expected error %s", err)
	}

	config.Set(TemplatePathPropertyName, "path_does_not_exist.json")
	_, err = NewHandler(config, nil, nil, nil)
	if err == nil {
		t.Error("Error was expected", err)
	}
}

func TestCallback(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(okGet))
	defer server.Close()

	baseURL, _ := url.Parse(server.URL)

	c := reb.Client{
		BaseURL: baseURL,
	}

	tpl, _ := model.ReadTemplate("testdata/template.json")
	handler := newHandler(tpl)
	handler.rebclient = &c

	msg := message{
		payload: []byte("ff90f15a-3254-4b01-90e6-d701748f0833"),
	}

	handler.Callback(nil, &msg)

	as, _ := handler.Repo.GetAllAgreements()
	if len(as) != 1 {
		t.Fatalf("Error in len(allAgrements). Expected: 1. Actual: %d", len(as))
	}

	writeAgreement(&as[0])
}

func TestCreateAgreement(t *testing.T) {
	tpl, _ := model.ReadTemplate("testdata/template.json")
	handler := newHandler(tpl)
	r := readReservation(reservationPath)
	a, err := handler.createAgreement(r)
	if err != nil {
		t.Errorf("Error creating agreement: %s", err)
	}
	as, _ := handler.Repo.GetAllAgreements()
	if len(as) != 1 {
		t.Errorf("Error in len(allAgrements). Expected: 1. Actual: %d", len(as))
	}
	if as[0].Id == "" && as[0].Id != a.Id {
		t.Errorf("Error in agreement ID. Expected: %s Actual: %s", a.Id, as[0].Id)
	}

	writeAgreement(a)
}

func TestGenAgreement(t *testing.T) {
	tpl, _ := model.ReadTemplate("testdata/template.json")
	handler := newHandler(tpl)
	r := readReservation(reservationPath)

	a, err := handler.genAgreement(r)

	if err != nil {
		t.Errorf("Error generating agreement: %s\n", err)
	}
	writeAgreement(a)
}

func TestVarsFromReservation(t *testing.T) {

	r := readReservation(reservationPath)

	actual := varsFromReservation(r)

	expected := map[string]interface{}{
		id:           "ff90f15a-3254-4b01-90e6-d701748f0832",
		providerID:   "ae061a09-ee75-4bbd-a13f-da01959ea705",
		providerName: "FBK",
		clientID:     "ca30f1ee-6c54-4b01-c0e6-c701748f0833",
		clientName:   "CreateNet",
		availability: "99.9",
	}

	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Not expected vars. Expected=%v Actual=%v", expected, actual)
	}
}

func TestParseAvailability(t *testing.T) {
	checkAvail(t, "99.9", "99,9%")
	checkAvail(t, "99.99", "99,99%")
	checkAvail(t, "99.99999", "99,99999%")
	checkAvail(t, "99.9", "99.9%")
	checkAvail(t, "99.9", "99,9")
	checkAvail(t, "99.9", "99.9")
}

func checkAvail(t *testing.T, expected string, src string) {
	actual := parseAvailability(src)
	if expected != actual {
		t.Errorf("Error parsing %s. Expected=%s Actual=%s ", src, expected, actual)
	}
}

func readReservation(path string) reb.Reservation {
	reader, _ := os.Open(path)
	var r reb.Reservation
	_ = json.NewDecoder(reader).Decode(&r)
	return r
}

func writeAgreement(a *model.Agreement) {
	enc := json.NewEncoder(os.Stdout)
	enc.SetEscapeHTML(false)
	enc.SetIndent(" ", " ")
	enc.Encode(a)
}

// okGet returns 200 and in body a reservation
func okGet(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	b, _ := ioutil.ReadFile(reservation2Path)
	w.Write(b)
}

type message struct {
	duplicate bool
	qos       byte
	retained  bool
	topic     string
	messageID uint16
	payload   []byte
	once      sync.Once
	ack       func()
}

func (m *message) Duplicate() bool {
	return m.duplicate
}

func (m *message) Qos() byte {
	return m.qos
}

func (m *message) Retained() bool {
	return m.retained
}

func (m *message) Topic() string {
	return m.topic
}

func (m *message) MessageID() uint16 {
	return m.messageID
}

func (m *message) Payload() []byte {
	return m.payload
}

func (m *message) Ack() {
	m.once.Do(m.ack)
}
