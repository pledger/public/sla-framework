//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

// Package mqtt subscribes to an external mqtt to get notifications of new reservations
package mqtt

import (
	"time"

	paho "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

const (
	// URLPropertyName is the property name for the url of the broker
	URLPropertyName = "brokerUrl"
	defaultURL      = "tcp://localhost:1883"

	// TopicPropertyName is the property name of the default topic to listen to
	TopicPropertyName = "brokerTopic"
	defaultTopic      = "decenter/reservations/confirmed"

	// UserPropertyName is the property name of the user to connect to the mqtt
	// (empty means no user)
	UserPropertyName = "brokerUser"
	defaultUser      = ""

	// PwdPropertyName is the property name of the pwd to connect to the mqtt
	// (empty means no password)
	PwdPropertyName = "brokerPassword"
	defaultPwd      = ""

	// RetrySeconds is the property name of the number of seconds to retry a connection
	RetrySeconds = "brokerRetry"
	defaultRetry = 60
)

// Subscriber is the struct that subscribes to an mqtt
type Subscriber struct {
	callback     paho.MessageHandler
	BrokerURL    string
	Topic        string
	User         string
	Password     string
	RetrySeconds int
}

// New constructs a MqttSubscriber
func New(config *viper.Viper, callback paho.MessageHandler) Subscriber {

	//config.SetDefault(URLPropertyName, defaultURL)
	//config.SetDefault(TopicPropertyName, defaultTopic)
	config.SetDefault(UserPropertyName, defaultUser)
	config.SetDefault(PwdPropertyName, defaultPwd)
	config.SetDefault(RetrySeconds, defaultRetry)

	if config.GetInt(RetrySeconds) == 0 {
		config.Set(RetrySeconds, defaultRetry)
	}

	logConfig(config)
	return Subscriber{
		callback:     callback,
		BrokerURL:    config.GetString(URLPropertyName),
		Topic:        config.GetString(TopicPropertyName),
		User:         config.GetString(UserPropertyName),
		Password:     config.GetString(PwdPropertyName),
		RetrySeconds: config.GetInt(RetrySeconds),
	}
}

func logConfig(config *viper.Viper) {
	log.Printf("MqttSubscriber configuration\n"+
		"\t%s: %v\n"+
		"\t%s: %v\n"+
		"\t%s: %v\n"+
		"\t%s: %v\n"+
		"\t%s: %d\n",
		URLPropertyName, config.GetString(URLPropertyName),
		TopicPropertyName, config.GetString(TopicPropertyName),
		UserPropertyName, config.GetString(UserPropertyName),
		PwdPropertyName, config.GetString(PwdPropertyName),
		RetrySeconds, config.GetInt(RetrySeconds))
}

// Subscribe connects to the
func (s *Subscriber) Subscribe() {

	ticker := time.NewTicker(time.Duration(s.RetrySeconds) * time.Second)

	for {
		if s.subscribe() {
			ticker.Stop()
		}
		<-ticker.C
	}
}

func (s *Subscriber) subscribe() bool {
	opts := paho.NewClientOptions().AddBroker(s.BrokerURL)

	c := paho.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		log.Infof("Cannot connect to mqtt (%s). Retrying in %d seconds\n", token.Error(), s.RetrySeconds)
		return false
	}

	if token := c.Subscribe(s.Topic, 0, s.callback); token.Wait() && token.Error() != nil {
		log.Infof("Cannot subscribe to mqtt (%s). Retrying in %d seconds\n", token.Error(), s.RetrySeconds)
		c.Disconnect(1000)
		return false
	}
	log.Infof("Subscribed to topic %s\n", s.Topic)
	return true
}
