//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package mqtt

import (
	"atos.pledger/sla-framework/connectors/reb"
	"atos.pledger/sla-framework/generator"
	"atos.pledger/sla-framework/model"
	"strconv"
	"strings"

	paho "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

const (
	// TemplatePathPropertyName is the property name of the template path to create agreements
	// in Decenter
	TemplatePathPropertyName = "decenterTemplatePath"
	defaultTemplatePath      = "resources/decenter-template.json"
	id                       = "id"
	providerID               = "provider_id"
	providerName             = "provider_name"
	clientID                 = "client_id"
	clientName               = "client_name"
	availability             = "avail"
)

// AgreementCreator is the struct with the logic
// to create an agreement from a reservation ID
type AgreementCreator struct {
	rebclient *reb.Client
	Template  *model.Template
	Repo      model.IRepository
	Val       model.Validator
}

// NewHandler creates an mqtt message handler that creates an agreement
// from the reservation ID received in the mqtt message
func NewHandler(config *viper.Viper, client *reb.Client, repo model.IRepository, val model.Validator) (AgreementCreator, error) {
	config.SetDefault(TemplatePathPropertyName, defaultTemplatePath)

	logConfigHandler(config)
	tpl, err := model.ReadTemplate(config.GetString(TemplatePathPropertyName))
	if err != nil {
		return AgreementCreator{}, err
	}
	result := AgreementCreator{
		rebclient: client,
		Repo:      repo,
		Val:       val,
		Template:  &tpl,
	}
	return result, nil
}

func logConfigHandler(config *viper.Viper) {
	log.Printf("AgreementCreator configuration\n"+
		"\t%s: %s\n",
		TemplatePathPropertyName, config.GetString(TemplatePathPropertyName))
}

// Callback retrieves a reservation given the ID in the message and creates
// an agreement based on the configured template.
func (o *AgreementCreator) Callback(client paho.Client, msg paho.Message) {
	reservationID := string(msg.Payload())
	reservation, err := o.rebclient.GetReservation(reservationID)
	if err != nil {
		log.Errorf("Error retrieving reservation %s. err=%s\n", reservationID, err)
	}
	_, err = o.createAgreement(reservation)
	if err != nil {
		log.Errorf("Error creating agreement from template: %s", err)
	}
}

func (o *AgreementCreator) createAgreement(r reb.Reservation) (*model.Agreement, error) {
	a, err := o.genAgreement(r)
	if err != nil {
		return &model.Agreement{}, err
	}
	created, err := o.Repo.CreateAgreement(a)
	if err != nil {
		return &model.Agreement{}, err
	}
	return created, nil
}
func (o *AgreementCreator) genAgreement(r reb.Reservation) (*model.Agreement, error) {
	genmodel := generator.Model{
		Template:  *o.Template,
		Variables: varsFromReservation(r),
	}
	return generator.Do(&genmodel, o.Val, false)
}

func varsFromReservation(r reb.Reservation) map[string]interface{} {
	return map[string]interface{}{
		id:           r.ID,
		providerID:   r.Seller.ID,
		providerName: r.Seller.Name,
		clientID:     r.Buyer.ID,
		clientName:   r.Buyer.Name,
		availability: parseAvailability(r.Slo.Limit),
	}
}

// parseAvailability transform the limit of availability in
// form "99,9%" to "99.9"
func parseAvailability(slo string) string {
	slo = strings.Replace(slo, ",", ".", 1)
	slo = strings.Replace(slo, "%", "", 1)
	f, err := strconv.ParseFloat(slo, 64)
	if err != nil {
		return ""
	}
	return strconv.FormatFloat(f, 'f', -1, 32)
}
