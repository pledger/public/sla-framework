//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package kafka

import (
 	"encoding/json"
	"fmt"
	"strings"
	"time"
	"reflect"

	"atos.pledger/sla-framework/assessment/notifier/kafka"
	myhttp "atos.pledger/sla-framework/common/http"
	log "atos.pledger/sla-framework/common/logs"
	"atos.pledger/sla-framework/model"

	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "SLA-Framework > Connectors > Kafka Eventhandler "

var KAFKA_AGREEMENT_TOPIC string
var KAFKA_PUBLISH_AGREEMENT_TOPIC string
var KAFKA_CLIENT string
var CONFIGSVC_ENDPOINT string
var CONFIGSVC_CREDENTIALS string

// Time from the last token generated for ConfigService API
var tokenTimeStamp time.Time
var tokenServerUrl string
var tokenServerUser string
var tokenServerPass string
var tokenCache string

/*
NewEventHandler
*/
func NewEventHandler() {
	var config = viper.New()
	config.SetEnvPrefix("SLA")
	config.AutomaticEnv()

	KAFKA_PUBLISH_AGREEMENT_TOPIC = "sla_contracts"
	KAFKA_AGREEMENT_TOPIC = config.GetString("KAFKA_AGREEMENT_TOPIC")
	KAFKA_CLIENT = config.GetString("KAFKA_CLIENT")
	CONFIGSVC_ENDPOINT = config.GetString("CONFIGSVC_ENDPOINT")
	CONFIGSVC_CREDENTIALS = config.GetString("CONFIGSVC_CREDENTIALS")
	
	switch KAFKA_CLIENT {
	case "kafka2":
		kafka.New2(config)
	case "kafka":
		//kafka.New(config)
	default:
		return
	}

	if KAFKA_AGREEMENT_TOPIC != "" && KAFKA_CLIENT != "" {
		go kafkaEventsLoop()
	}
}

/*
   Kafka events handler
*/
func kafkaEventsLoop() {
	log.Println(pathLOG + "[kafkaEventsLoop] Starting to listen topic " + KAFKA_AGREEMENT_TOPIC)
	for {
		var message string
		var err error

		switch KAFKA_CLIENT {
		case "kafka2", "kafka":
			message, err = kafka.CheckForMessages(KAFKA_AGREEMENT_TOPIC)
		default:
			break
		}
		if err != nil {
			log.Error(pathLOG+"[kafkaEventsLoop] Error reading kafka event ...", err)
			continue
		}

		// Message processing ...
		log.Printf(pathLOG+"[kafkaEventsLoop] Event received from kafka: %s\n", message)
		// Message is a JSON with information of agreement
		// We expect something like this: {'id': 1, 'entity': 'sla', 'operation': 'update'}
		// And we need to call ConfService REST API /api/slas/{id}
		var eventObj interface{}
		err = json.Unmarshal([]byte(message), &eventObj)
		if err != nil {
			log.Error(pathLOG+"[kafkaEventsLoop] Error unmarshalling JSON message: ", err)
			continue
		}
		eventInfoMap := eventObj.(map[string]interface{})
		//agreementID := eventInfoMap["id"].(string)
		agreementIDn := eventInfoMap["id"].(float64)
		if agreementIDn == 0 {
			continue
		}
		agreementID := fmt.Sprintf("%d", uint64(agreementIDn))
		if agreementID == "" {
			continue
		}
		operation := eventInfoMap["operation"].(string)
		if operation == "" {
			continue
		}
		entity := eventInfoMap["entity"].(string)
		if entity == "" || entity != "sla" {
			continue
		}

		log.Println(pathLOG + "[kafkaEventsLoop] Processing operation ...")
		if operation == "update" {
			var hasAuth bool = false
			var token string
			if CONFIGSVC_CREDENTIALS != "" {
				token = checkTokenConfigSvc(CONFIGSVC_CREDENTIALS)
				hasAuth = true
			}

			// Call ConfigService API for agreement info
			log.Println(pathLOG + "[kafkaEventsLoop] Calling ConfigService API for agreement info [" + CONFIGSVC_ENDPOINT+"slas/"+agreementID + "] ...")
			code, body, err := myhttp.GetString(CONFIGSVC_ENDPOINT+"slas/"+agreementID, hasAuth, token)
			if err != nil || code != 200 {
				log.Error(pathLOG+"[kafkaEventsLoop] Error calling ConfigService API for agreement info: ", err)
				continue
			}
			log.Println(pathLOG + "[kafkaEventsLoop] Aggrement info: " + body)
			var agreementInfo interface{}
			err = json.Unmarshal([]byte(body), &agreementInfo)
			if err != nil || agreementInfo == nil {
				log.Error(pathLOG+"[kafkaEventsLoop] Error unmarshalling JSON agreement info: ", err)
				continue
			}
			agreementInfoMap := agreementInfo.(map[string]interface{})

			// Call ConfigService API for guarantees info
			log.Println(pathLOG + "[kafkaEventsLoop] Calling ConfigService API for guarantees info [" + CONFIGSVC_ENDPOINT+"slas/"+agreementID + "] ...")
			code, body2, err := myhttp.GetString(CONFIGSVC_ENDPOINT+"guarantees/sla/"+agreementID, hasAuth, token)
			if err != nil || code != 200 {
				log.Error(pathLOG+"[kafkaEventsLoop] Error calling ConfigService API for guarantees info: ", err)
				continue
			}
			log.Println(pathLOG + "[kafkaEventsLoop] Guarantees info: " + body2)
			var guaranteesInfo interface{}
			err = json.Unmarshal([]byte(body2), &guaranteesInfo)
			if err != nil || guaranteesInfo == nil {
				log.Error(pathLOG+"[kafkaEventsLoop] Error unmarshalling JSON guarantees info: ", err)
				continue
			}

			// Process information and create SLA
			log.Println(pathLOG + "[kafkaEventsLoop] Creating SLA from gathered information ...")

			if agreementInfoMap["infrastructureProvider"] == nil {
				log.Error(pathLOG+"[kafkaEventsLoop] Error getting content for 'infrastructureProvider': ", err)
				continue
			} else if agreementInfoMap["serviceProvider"] == nil {
				log.Error(pathLOG+"[kafkaEventsLoop] Error getting content for 'serviceProvider': ", err)
				continue
			} else if agreementInfoMap["service"] == nil {
				log.Error(pathLOG+"[kafkaEventsLoop] Error getting content for 'app': ", err)
				continue
			}

			// prepare and check structs
			infrastructureInfoMap := agreementInfoMap["infrastructureProvider"].(map[string]interface{})
			serviceInfoMap := agreementInfoMap["serviceProvider"].(map[string]interface{})
			appInfoMap := agreementInfoMap["service"].(map[string]interface{})
			// guarantees
			if IsSlice(guaranteesInfo) == false {
				log.Error(pathLOG+"[kafkaEventsLoop] 'guaranteesInfo' is not a slice object")
				continue
			} 
			guaranteeSets := guaranteesInfo.([]interface{})
			
			// generate agreement
			agreement, err := generateNewSLA(agreementInfoMap, appInfoMap, infrastructureInfoMap, serviceInfoMap, guaranteeSets)

			// Process information and create or update the SLA
			log.Println(pathLOG + "[kafkaEventsLoop] Checking if SLA already exists ...")
			err = getSLA(agreementID)
			if err != nil {
				// create a new one
				log.Println(pathLOG + "[kafkaEventsLoop] Creating new SLA ...")
				createSLA(agreement)
			} else {
				// update = delete and create
				log.Println(pathLOG + "[kafkaEventsLoop] Updating SLA ...")
				log.Println(pathLOG + "[kafkaEventsLoop] 1. Removing existing SLA ...")
				deleteSLA(agreementID)
				time.Sleep(5 * time.Second)
				log.Println(pathLOG + "[kafkaEventsLoop] 2. Creating SLA ...")
				createSLA(agreement)
			}
		} 
		if operation == "delete" {
			// DELETE operation
			log.Println(pathLOG + "[kafkaEventsLoop] Deleting SLA ...")
			deleteSLA(agreementID)
		} 
	}
}

/*
   Get a valid token for ConfigService (token expires every 24 hours)
   CONFIGSVC_CREDENTIALS="url:http://ip:port/api/authenticate,username:xxxxx,password:xxxxx"
*/
func checkTokenConfigSvc(credentials string) string {
	var token string = ""
	if tokenServerUrl == "" {
		params := strings.SplitN(credentials, ",", 3)
		for idx := range params {
			param := strings.SplitN(params[idx], ":", 2)
			switch param[0] {
			case "url":
				tokenServerUrl = param[1]
			case "username":
				tokenServerUser = param[1]
			case "password":
				tokenServerPass = param[1]
			case "token": // Non expiration token
				token = param[1]
			}
		} // for params
	}
	if token != "" { // Non expiration token
		return token
	}
	if time.Since(tokenTimeStamp).Hours() >= 24 {
		payload := fmt.Sprintf("{\"username\":\"%s\", \"password\":\"%s\"}", tokenServerUser, tokenServerPass)
		//code, body, err := myhttp.GetWithPayload(tokenServerUrl, false, "", payload)
		//code, body, err := myhttp.Post(tokenServerUrl, false, "", payload)
		code, body, err := myhttp.PostRawData(tokenServerUrl, false, "", payload)
		if err != nil || code != 200 {
			log.Error(pathLOG+"[checkTokenConfigSvc] Error calling ConfigService API for new API token: ", err, payload)
			return token
		}
		var tokenJsonMsg map[string]string
		err = json.Unmarshal([]byte(body), &tokenJsonMsg)
		if err != nil {
			return token
		}
		token = tokenJsonMsg["id_token"]
		if token != "" {
			tokenTimeStamp = time.Now()
			tokenCache = token
		} else {
			log.Error(pathLOG+"[checkTokenConfigSvc] Error getting ConfigService API new token: ", body)
		}
	} else {
		token = tokenCache
	}
	return token
}

// 
func createSLA(agreement model.Agreement) error {
	// saving agreement / SLA
	log.Println(pathLOG + "[createSLA] Saving SLA ...")
	content, err := json.Marshal(agreement)
	if err != nil {
		log.Error(pathLOG+"[createSLA] New Aggrement error: ", err)
	}
	log.Println(pathLOG + "[createSLA] New Aggrement: " + string(content))

	code, _, err := myhttp.Post("http://localhost:8090/agreements", false, "", agreement)
	if err != nil || code >= 400 { // 201 Created
		log.Error(pathLOG+"[createSLA] Error calling SLA API for new agreement: ", err)
	} else {
		log.Println(pathLOG + "[createSLA] SLA created and saved. Sending SLA creation message to Kafka ...")
		
		_, err = kafka.PublishMessage(KAFKA_PUBLISH_AGREEMENT_TOPIC, string(content))
		if err != nil {
			log.Error(pathLOG+"[createSLA] Error (1) sending SLA creation message to Kafka: ", err)
			log.Println(pathLOG + "[createSLA] Sending again SLA creation message to Kafka ...")
			time.Sleep(15 * time.Second)
			_, err = kafka.PublishMessage(KAFKA_PUBLISH_AGREEMENT_TOPIC, string(content))
			if err != nil {
				log.Error(pathLOG+"[createSLA] Error (2) sending message to Kafka: ", err)
			}
		}
		
	}
	return err
}

// 
func deleteSLA(agreementID string) error {
	log.Println(pathLOG + "[deleteSLA] Deleting SLA ...")
	code, _, err := myhttp.Delete("http://localhost:8090/agreements/"+agreementID, false, "", nil)
	if err != nil || code >= 400 { // 204 No Content
		log.Error(pathLOG+"[deleteSLA] Error calling SLA API for delete agreement: ", err)
	}
	return err
}

// 
func getSLA(agreementID string) error {
	log.Println(pathLOG + "[getSLA] Getting SLA ...")
	code, _, err := myhttp.Get("http://localhost:8090/agreements/"+agreementID, false, "")
	if err != nil || code != 200 { // 204 No Content
		log.Warn(pathLOG+"[getSLA] Error calling SLA API to get agreement / SLA not found: ", err)
	}
	return err
}

// generateNewSLA
func generateNewSLA(agreementInfoMap map[string]interface{}, appInfoMap map[string]interface{}, 
					infrastructureInfoMap map[string]interface{}, serviceInfoMap map[string]interface{}, 
					guaranteeSets []interface{}) (model.Agreement, error) {
	var agreement model.Agreement

	//agreement.Id = agreementInfoMap["id"].(string)
	agreementIdn := agreementInfoMap["id"].(float64)
	agreement.Id = fmt.Sprintf("%d", uint64(agreementIdn))
	agreement.Details.Id = agreement.Id
	agreement.Name = agreementInfoMap["name"].(string)
	agreement.Details.Name = agreement.Name
	agreement.Details.Type = "agreement"
	agreement.State = "started"
	//agreement.Details.Service = appInfoMap["id"].(string)
	servideIdn := appInfoMap["id"].(float64)
	agreement.Details.Service = fmt.Sprintf("%d", uint64(servideIdn))
	//agreement.Details.Creation = agreementInfoMap["creation"].(time.Time)
	//agreement.Details.Expiration = agreementInfoMap["expiration"].(*time.Time)
	//agreement.Details.Provider.Id = infrastructureInfoMap["id"].(string)
	providerIdn := infrastructureInfoMap["id"].(float64)
	agreement.Details.Provider.Id = fmt.Sprintf("%d", uint64(providerIdn))
	agreement.Details.Provider.Name = infrastructureInfoMap["name"].(string)
	//agreement.Details.Client.Id = serviceInfoMap["id"].(string)
	clientIdn := serviceInfoMap["id"].(float64)
	agreement.Details.Client.Id = fmt.Sprintf("%d", uint64(clientIdn))
	agreement.Details.Client.Name = serviceInfoMap["name"].(string)

	for key := range guaranteeSets {
		value := guaranteeSets[key].(map[string]interface{})
		var guarantee model.Guarantee
		var importance model.GuaranteeType

		guaranteeIdn := value["id"].(float64)
		guarantee.Name = fmt.Sprintf("%d", uint64(guaranteeIdn))
		guarantee.Constraint = value["constraint"].(string)
		
		if value["thresholdWarning"] != nil {
			importance.Name = "Warning"
			importance.Constraint = value["thresholdWarning"].(string)
			guarantee.Importance = append(guarantee.Importance, importance)
		}
		
		if value["thresholdMild"] != nil {
			importance.Name = "Mild"
			importance.Constraint = value["thresholdMild"].(string)
			guarantee.Importance = append(guarantee.Importance, importance)
		}
		
		if value["thresholdSerious"] != nil {
			importance.Name = "Serious"
			importance.Constraint = value["thresholdSerious"].(string)
			guarantee.Importance = append(guarantee.Importance, importance)
		}
		
		if value["thresholdSevere"] != nil {
			importance.Name = "Severe"
			importance.Constraint = value["thresholdSevere"].(string)
			guarantee.Importance = append(guarantee.Importance, importance)
		}
		
		if value["thresholdCatastrophic"] != nil {
			importance.Name = "Cathastrophic"
			importance.Constraint = value["thresholdCatastrophic"].(string)
			guarantee.Importance = append(guarantee.Importance, importance)
			agreement.Details.Guarantees = append(agreement.Details.Guarantees, guarantee)
		}
	}

	return agreement, nil
}

// IsSlice check if interface is a slice
func IsSlice(v interface{}) bool {
	// reflect.TypeOf(v).Kind() == reflect.Array
    return reflect.TypeOf(v).Kind() == reflect.Slice
}