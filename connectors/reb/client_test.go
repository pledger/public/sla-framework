//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package reb

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	"github.com/spf13/viper"
)

func TestNew(t *testing.T) {
	defaultBase := "http://localhost:8080"
	cfg := viper.New()
	cfg.Set(URLPropertyName, defaultBase)
	client := New(cfg)
	expected := defaultBase + "/" + DefaultRootPath
	actual := client.BaseURL.String()
	if actual != expected {
		t.Errorf("Unexpected notification URL: expected=%s actual=%s", expected, actual)
	}

	newRootPath := "/v1"
	cfg.Set(PathPropertyName, newRootPath)
	client = New(cfg)
	expected = defaultBase + newRootPath
	actual = client.BaseURL.String()
	if actual != expected {
		t.Errorf("Unexpected notification URL: expected=%s actual=%s", expected, actual)
	}
}

func TestReservationsUrl(t *testing.T) {

	base, err := url.Parse("https://localhost:8080")
	if err != nil {
		t.Fatal(err.Error())
	}
	actual := reservationsURL(base, "id")

	expected := "https://localhost:8080/reservations/id"
	if actual != expected {
		t.Errorf("Error in reservationsURL; expected=%s actual=%s", expected, actual)
	}
}

func TestFakeSendMessage(t *testing.T) {
	testServerPatch(t, false, okPatch)
	testServerPatch(t, true, _404)
}

func TestHttpError(t *testing.T) {
	baseURL, _ := url.Parse("http://localhost:1")
	c := Client{
		BaseURL: baseURL,
	}
	err := c.PatchReservation("an-id", Fullfilled)
	if err == nil {
		t.Error("An error was expected")
	}

	_, err = c.GetReservation("an-id")
	if err == nil {
		t.Error("An error was expected")
	}
}

func TestFakeGet(t *testing.T) {
	testServerGet(t, false, okGet)
	testServerGet(t, true, _404)
}

func testServerPatch(t *testing.T, errorExpected bool, f func(w http.ResponseWriter, r *http.Request)) {

	server := httptest.NewServer(http.HandlerFunc(f))
	defer server.Close()

	baseURL, _ := url.Parse(server.URL)

	c := Client{
		BaseURL: baseURL,
	}
	err := c.PatchReservation("an-id", Fullfilled)
	if err != nil && !errorExpected {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	if err == nil && errorExpected {
		t.Error("An error was expected")
	}
}

func testServerGet(t *testing.T, errorExpected bool, f func(w http.ResponseWriter, r *http.Request)) {

	server := httptest.NewServer(http.HandlerFunc(f))
	defer server.Close()

	baseURL, _ := url.Parse(server.URL)

	c := Client{
		BaseURL: baseURL,
	}
	actual, err := c.GetReservation("an-id")
	if err != nil && !errorExpected {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	if err == nil && errorExpected {
		t.Error("An error was expected")
	}

	if errorExpected {
		return
	}

	reader, _ := os.Open("testdata/reservation.json")
	var expected Reservation
	_ = json.NewDecoder(reader).Decode(&expected)

	if expected != actual {
		t.Fatalf("Not valid returned reservation: %v", actual)
	}
}

func okPatch(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func okGet(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	b, _ := ioutil.ReadFile("testdata/reservation.json")
	w.Write(b)
}

func _404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}
