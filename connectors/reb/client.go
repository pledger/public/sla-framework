//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

/*
Package reb implements the REST client to a REB. The only relevant entity
for the SLAs is the reservation, which has the following format:

	{
	"id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
	"buyerAdminDomain": {
		"id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
		"name": "string",
	},
	"sellerAdminDomain": {
		"id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
		"name": "string",
	},
	"duration": 3600,
	"timestamp": {},
	"advertisementId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
	"totalCost": 0,
	"slo": {
		"id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
		"name": "string",
		"description": "string",
		"limit": "string"
	},
	"token": "string",
	"status": "created"
	}


Non relevant fields have been supressed.

Full API is documented at
https://app.swaggerhub.com/apis/decenter/decenter-resource-sharing-api/1.1.0-alpha#

*/
package reb

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	// URLPropertyName is the name of the property rebUrl
	URLPropertyName = "rebUrl"

	// PathPropertyName is the name of the property rebPath
	PathPropertyName = "rebPath"

	// DefaultRootPath is the root path of the REB REST API
	DefaultRootPath = "decenter/decenter-resource-sharing-api/1.0.1"

	// Fullfilled is the status of a reservation after expiration without violations
	Fullfilled Status = "fulfilled"
	// NotFullfilled is the status of a reservation after a violation
	NotFullfilled Status = "unfulfilled"

	applicationJSON = "application/json"
	contentType     = "Content-type"
	accept          = "Accept"
)

// Client is the struct to connect to a REB endpoint
type Client struct {
	BaseURL *url.URL
	client  *http.Client
}

// Status is the type of a reservation status. It can be fulfilled or notfulfilled
type Status string

// Reservation is the type to use to marshall a request to /reservations
type Reservation struct {
	ID        string      `json:"id"`
	Buyer     AdminDomain `json:"buyerAdminDomain"`
	Seller    AdminDomain `json:"sellerAdminDomain"`
	Duration  int         `json:"duration"`
	Status    string      `json:"status"`
	TotalCost float64     `json:"totalCost"`
	Token     string      `json:"token"`
	Slo       Slo         `json:"slo"`
}

// AdminDomain is the type of a buyer domain or a seller domain
type AdminDomain struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Slo is the type that represents the SLO of a reservation
type Slo struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Limit       string `json:"limit"`
}

// patchReservationResponse is type type to use to unmarshall a response to PATCH /reservations/{id}
type patchReservationResponse struct {
	message string
}

// New constructs a REST Notifier
func New(config *viper.Viper) Client {

	baseurl, _ := url.Parse(config.GetString(URLPropertyName))

	var rootpath string
	if config.IsSet(PathPropertyName) {
		rootpath = config.GetString(PathPropertyName)
	} else {
		rootpath = DefaultRootPath
	}
	baseurl.Path = path.Join(baseurl.Path, rootpath)

	result := Client{
		BaseURL: baseurl,
	}

	logConfig(config, result)
	return result
}

func logConfig(config *viper.Viper, c Client) {
	log.Infof("RebClient configuration\n"+
		"\t%s: %v\n"+
		"\t%s: %v\n"+
		"\tBaseURL: %s\n",
		URLPropertyName, config.GetString(URLPropertyName),
		PathPropertyName, config.GetString(PathPropertyName),
		c.BaseURL.String())
}

func getClient(insecure bool) *http.Client {

	var netClient = &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: insecure,
			},
		},
	}
	return netClient
}

func (c *Client) getClient() *http.Client {
	if c.client == nil {
		c.client = getClient(false)
	}
	return c.client
}

// GetReservation returns a reservation
func (c *Client) GetReservation(id string) (Reservation, error) {

	url := reservationsURL(c.BaseURL, id)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set(accept, applicationJSON)

	resp, err := c.getClient().Do(req)
	if err != nil {
		return Reservation{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return Reservation{}, fmt.Errorf("Unexpected %d", resp.StatusCode)
	}

	var result Reservation
	err = json.NewDecoder(resp.Body).Decode(&result)

	return result, err
}

// PatchReservation updates the status of a reservation
func (c *Client) PatchReservation(id string, status Status) error {

	url := reservationsURL(c.BaseURL, id)

	jsonValue := map[string]interface{}{
		"status":    status,
		"totalCost": 0,
		"token":     "",
	}

	body, _ := json.Marshal(jsonValue)

	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewReader(body))
	req.Header.Set(contentType, applicationJSON)
	req.Header.Set(accept, applicationJSON)

	resp, err := c.getClient().Do(req)

	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		//var out reservationResponse
		//err := json.NewDecoder(resp.Body).Decode(out)
		return fmt.Errorf("Unexpected %d - %s", resp.StatusCode, string(body))
	}

	return nil
}

// reservationsURL returns the URL corresponding to a reservation
func reservationsURL(baseURL *url.URL, slaID string) string {
	tmp := *baseURL
	tmp.Path = path.Join(tmp.Path, "reservations", slaID)
	return tmp.String()
}
