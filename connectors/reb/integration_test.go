//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

/*
To run the integration test

	SLA_REBURL=http://localhost:8080 go test SLALite/decenter/rebclient

*/

package reb

import (
	"net/url"
	"os"
	"path"
	"testing"

	log "github.com/sirupsen/logrus"
)

const (
	RebURLEnv = "SLA_REBURL"
)

func checkRunIntegrationTest(t *testing.T) {
	_, ok := os.LookupEnv(RebURLEnv)

	if !ok {
		log.Info("Skipping integration test")
		t.SkipNow()
	}
	log.Info("Running integration test")
}

func rebURL() string {
	url, ok := os.LookupEnv(RebURLEnv)
	if !ok {
		return ""
	}
	return url
}

func TestPatch(t *testing.T) {
	checkRunIntegrationTest(t)

	baseURL, _ := url.Parse(rebURL())
	baseURL.Path = path.Join(baseURL.Path, DefaultRootPath)
	c := Client{
		BaseURL: baseURL,
	}

	err := c.PatchReservation("10f90f144-8654-4b01-90e6-d701748f0886", Fullfilled)
	if err != nil {
		t.Fatalf("err=%s", err)
	}
}

func TestGet(t *testing.T) {
	checkRunIntegrationTest(t)

	baseURL, _ := url.Parse(rebURL())
	baseURL.Path = path.Join(baseURL.Path, DefaultRootPath)
	c := Client{
		BaseURL: baseURL,
	}

	r, err := c.GetReservation("10f90f144-8654-4b01-90e6-d701748f0886")
	if err != nil {
		t.Fatalf("err=%s", err)
	}
	log.Infof("r=%#v\n", r)
}
