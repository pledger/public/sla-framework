#!/usr/bin/bash
export SLA_REPOSITORY=mongodb
export SLA_DATABASE=slalite
export SLA_DB_USER=root
export SLA_DB_PASS=password
export SLA_CONNECTION=mongodb://root:password@localhost:32002
export SLA_PROMETHEUSURL=http://localhost:9090
export SLA_ADAPTER=prometheus
export SLA_NOTIFIER=kafka2
export SLA_CONFIGSVC_ENDPOINT=http://localhost:8000/api/
export SLA_CONFIGSVC_CREDENTIALS="url:http://localhost:8000/api/authenticate,username:api,password:apiH2020"
export SLA_KAFKA_CLIENT=kafka2
export SLA_KAFKA_ENDPOINT=192.168.1.24:9095
export SLA_KAFKA_NOTIFIER_TOPIC=sla_violation
export SLA_KAFKA_AGREEMENT_TOPIC=sla_agreement
export SLA_KAFKA_KEYSTORE_PASSWORD=password
export SLA_KAFKA_KEY_PASSWORD=password
export SLA_KAFKA_TRUSTSTORE_PASSWORD=password
export SLA_KAFKA_KEYSTORE_LOCATION=$HOME/tmp/kafka/keystore/localdev/kafka.client.keystore.p12
export SLA_KAFKA_TRUSTSTORE_LOCATION=$HOME/tmp/kafka/truststore/localdev/kafka.client.truststore.pem
export TZ=Z
echo "go module tidy..."
go mod tidy
echo "go build..."
CGO_ENABLED=0 GOOS=linux go build -a -o SLALite .
echo "go run..."
#kubectl -n core port-forward --address localhost,0.0.0.0 service/slalite-db 32002:27017 > /dev/null 2>&1 &
#kubectl -n monitoring port-forward --address localhost,0.0.0.0 svc/kube-prometheus-stack-prometheus 9090:9090 > /dev/null 2>&1 &
#ulimit -n 4096
go run .
#./SLALite


