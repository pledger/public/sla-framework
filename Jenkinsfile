pipeline {
    agent any
    //tools {
    //}
    environment {
      APP_NAME = "slalite"
      DB_NAME = "slalite-db"
      UI_NAME = "slalite-ui"
      ARTIFACTORY_SERVER = "https://116.203.2.204:443/artifactory/plgregistry/"
      ARTIFACTORY_DOCKER_REGISTRY = "116.203.2.204:443/plgregistry/"
      BRANCH_NAME = "master"
      DOCKER_APP_IMAGE_TAG = "$APP_NAME:R${env.BUILD_ID}"
      DOCKER_DB_IMAGE_TAG = "$DB_NAME:R${env.BUILD_ID}"
      DOCKER_UI_IMAGE_TAG = "$UI_NAME:R${env.BUILD_ID}"
      IMAGE_TAG = "R${env.BUILD_ID}"
      PORT_MAPPING_APP = "8090:8090"
      PORT_MAPPING_DB  = "27017:27017"
      PORT_MAPPING_UI  = "8091:8000"
      DEPLOYMENT_FILE = "slalite-app.yaml"
      VM_DEV01 = "116.203.2.205:2376"
      VM_DEV02 = "116.203.2.206:2376"
    }

    stages {

        stage('Checkout') {
           steps {
              echo 'Checkout SCM'
              checkout scm
              checkout([$class: 'GitSCM',
                        branches: [[name: env.BRANCH_NAME]],
                        extensions: [[$class: 'CleanBeforeCheckout']],
                        userRemoteConfigs: scm.userRemoteConfigs
              ])
           }
        }

        stage('Build with Go') {	// make build
            steps {
                echo 'Build with Go: make build'
                //sh 'cd sla-framework && make build'
            }
        }
		
        stage('Test with Go') { // test
            steps {
                echo 'Starting to test: make test'
                //sh 'cd sla-framework && make test'
            }
        }		

        stage('Build images') { // build and tag docker image
            steps {
                echo 'Starting to build docker image'
                script {
                    sh 'docker image ls -a'

                    // APP image
                    sh 'chmod 744 resources/bin/make_docker.sh && resources/bin/make_docker.sh $APP_NAME' 
                    sh 'docker image prune --filter label=stage=builder --filter label=build=$BUILD_UI_ID'
                    sh 'VERSION=$(git describe --always --dirty | sed -e"s/^v//") && docker tag $APP_NAME:$VERSION $APP_NAME:latest' 
                    sh 'docker tag $APP_NAME "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_APP_IMAGE_TAG"'

                    // UI image 
                    sh 'cd UI && docker build --rm -t $UI_NAME .'
                    sh 'docker image prune --filter label=stage=builder --filter label=build=$BUILD_UI_ID'
                    sh 'docker tag $UI_NAME "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_UI_IMAGE_TAG"'

                    sh 'docker rmi $(docker images -f dangling=true -q)'
                    sh 'docker image ls -a'
                }
            }
        }


        stage ('Push images to Artifactory') {
            steps {
              withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifacts', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                  sh 'docker image ls -a'
                  echo 'Login to Artifactory Registry'
                  sh "docker login --password=${PASSWORD} --username=${USERNAME} ${ARTIFACTORY_SERVER}"

                  echo 'Pull image with Build-ID'
                  sh 'docker push "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_APP_IMAGE_TAG"'
                  sh 'docker push "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_UI_IMAGE_TAG"'

                  echo 'Logout from Registry'
                  sh 'docker logout $ARTIFACTORY_SERVER'
                  sh 'docker image ls -a'
              }
            }
        }


        stage('Docker Remove images from CI Server') {
           steps {
                sh 'docker image ls -a'

                // APP image
                sh 'docker rmi "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_APP_IMAGE_TAG"'
                sh 'docker rmi "$APP_NAME:latest"'
                sh 'VERSION=$(git describe --always --dirty | sed -e"s/^v//") && docker rmi "$APP_NAME:$VERSION"'

                // UI image
                sh 'docker rmi "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_UI_IMAGE_TAG"'
                sh 'docker rmi "$UI_NAME:latest"'

                sh 'docker image ls -a'
           }
        }


        stage('Remove containers from VM-DEV01 (duplicate)') { 
          steps { 
            script { 
              docker.withServer("$VM_DEV01", 'vm-dev01-creds') { 
                sh 'docker image ls -a'
                sh 'CONTAINER_ID=$(docker ps -a |grep $APP_NAME|awk \'{print $1;}\') && if [ -n "$CONTAINER_ID" ]; then docker stop $CONTAINER_ID; docker rm $CONTAINER_ID; fi' 
                sh 'CONTAINER_ID=$(docker ps -a |grep $UI_NAME|awk \'{print $1;}\') && if [ -n "$CONTAINER_ID" ]; then docker stop $CONTAINER_ID; docker rm $CONTAINER_ID; fi'
                sh 'docker system prune -a -f' 
                sh 'docker image ls -a' 
              } 
            } 
          } 
        } 


        stage('Deploy images on DEV_XX') {
          steps{
            script {
              docker.withServer("$VM_DEV01", 'vm-dev01-creds') {
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifacts', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                    echo 'Login to Artifactory Registry'
                    sh "docker login --password=${PASSWORD} --username=${USERNAME} ${ARTIFACTORY_SERVER}"

                    echo 'Pull image with Build-ID'
                    sh 'docker pull "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_APP_IMAGE_TAG"'
                    sh 'docker pull "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_UI_IMAGE_TAG"'

                    echo 'Run docker image in detach mode'
                    sh 'docker run -d -p $PORT_MAPPING_APP --name "$APP_NAME" "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_APP_IMAGE_TAG"'
                    sh 'docker run -d -p $PORT_MAPPING_UI --name "$UI_NAME" "$ARTIFACTORY_DOCKER_REGISTRY$DOCKER_UI_IMAGE_TAG"' 

                    echo 'Logout from Registry'
                    sh 'docker logout $ARTIFACTORY_SERVER'
                }
              }
            }
          }
        }
		

        stage('Remove containers from VM-DEV01') { 
          steps { 
            script { 
              docker.withServer("$VM_DEV01", 'vm-dev01-creds') { 
                //sh 'docker stop $(docker ps -a |grep $APP_NAME|awk \'{print $1;}\')' 
                //sh 'docker rm $(docker ps -a |grep $APP_NAME|awk \'{print $1;}\')' 
                sh 'CONTAINER_ID=$(docker ps -a |grep $APP_NAME|awk \'{print $1;}\') && if [ -n "$CONTAINER_ID" ]; then docker stop $CONTAINER_ID; docker rm $CONTAINER_ID; fi' 
                sh 'CONTAINER_ID=$(docker ps -a |grep $UI_NAME|awk \'{print $1;}\') && if [ -n "$CONTAINER_ID" ]; then docker stop $CONTAINER_ID; docker rm $CONTAINER_ID; fi'
                sh 'docker system prune -a -f' 
              } 
            } 
          } 
        } 

/*		
	stage('Remove test-rest from VM-DEV02') { steps { script { docker.withServer("$VM_DEV02", 'vm-dev02-creds') { sh 'docker stop $(docker ps -a |grep $APP_NAME|awk \'{print $1;}\')' sh 'docker rm $(docker ps -a |grep $APP_NAME|awk \'{print $1;}\')' sh 'docker system prune -a -f' } } } }
*/


        stage('Deploy images on K8S') {
          steps {
            sh 'echo Deploying in K8S cluster'
            withKubeConfig([credentialsId: 'Jenkins_ServiceAccount' , serverUrl: 'https://192.168.70.5:6443/', namespace:'core']) {
              sh 'kubectl get all -o wide'
              //sh 'kubectl apply -f $DEPLOYMENT_FILE'
              //sh 'awk -v VERSION="$IMAGE_TAG" \'{if ($1=="image:" && $2 ~ /^116/) {print $0":"VERSION} else print $0}\' $DEPLOYMENT_FILE | kubectl apply -f -'
              sh 'sed -i "s/IMAGE_TAG/${IMAGE_TAG}/g" ./kustomizer/overlays/testbed/kustomization.yaml'
              sh 'kubectl kustomize ./kustomizer/overlays/testbed/'
              //sh 'kubectl delete -f ./kustomizer/overlays/testbed/' 
              sh 'kubectl apply -k ./kustomizer/overlays/testbed/'
              sh 'kubectl get all -o wide'
            }
          }	
        }
		
    }
}
