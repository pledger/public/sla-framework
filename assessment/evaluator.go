//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

// Package assessment contains the core code that evaluates the agreements.
package assessment

import (
	"fmt"
	"time"
	"strings"

	amodel "atos.pledger/sla-framework/assessment/model"
	"atos.pledger/sla-framework/assessment/monitor"
	"atos.pledger/sla-framework/assessment/notifier"
	"atos.pledger/sla-framework/model"

	log "atos.pledger/sla-framework/common/logs"
	"github.com/Knetic/govaluate"
)

// path used in logs
const pathLOG string = "SLA-Framework > Assessment [EVALUATOR] "

/* 
Config contains the configuration for an assessment process (i.e. global config)
*/
type Config struct {
	// Now is the time considered the current time. In general terms, metrics are retrieved from the adapter from the last measure to `now`.
	// If the monitoring have some delay storing metrics, now could be shifted some minutes to the past.
	Now time.Time

	// Repo is the repository where to load/store entities
	Repo model.IRepository

	// Adapter is the monitoring adapter where to get metrics from
	Adapter monitor.MonitoringAdapter

	// Notifier receives the violations and notifies them (send by REST, store to DB...)
	Notifier notifier.ViolationNotifier

	// Transient is time to wait until a new violation of a GT can be raised again (default value is zero)
	Transient time.Duration
}

/*
AssessActiveAgreements will get the active agreements from the provided repository and assess them, notifying about violations with the provided notifier.
Mandatory fields filled in cfg are Repo, Adapter and Now.
*/
func AssessActiveAgreements(cfg Config) {
	repo := cfg.Repo
	not := cfg.Notifier

	agreements, err := repo.GetAgreementsByState(model.STARTED, model.STOPPED)
	if err != nil {
		log.Error(pathLOG+"[AssessActiveAgreements] Error getting active agreements: %s", err.Error())
	} else {
		log.Printf(pathLOG+"[AssessActiveAgreements] AssessActiveAgreements(). [ %d agreements to evaluate]", len(agreements))
		for _, agreement := range agreements {
			result := AssessAgreement(&agreement, cfg)
			repo.UpdateAgreement(&agreement)
			if not != nil && len(result.Violated) > 0 {
				// notify violations
				not.NotifyViolations(&agreement, &result)

				// store violations in historical db
				storeViolations(agreement, result, repo)
			}
		}
	}
}

/*
AssessAgreement is the process that assess an agreement. The process is:
	1. Check expiration date
	2. Evaluate metrics if agreement is started
	3. Set LastExecution time.
The output is:
	- parameter a is modified
	- evaluation results are the function return (violated metrics and raised violations).
	- a guarantee term is filled in the result only if there are violations.
The function results are not persisted. The output must be persisted/handled accordingly.
E.g.: agreement and violations must be persisted to DB. Violations must be notified to observers
*/
func AssessAgreement(a *model.Agreement, cfg Config) amodel.Result {
	var result amodel.Result
	var err error

	now := cfg.Now

	log.Debug("-----------------------------------------------------------------------------------------")
	log.Trace(pathLOG+"[AssessAgreement] Assessing Agreement with ID: ", a.Id)
	if a.Details.Expiration != nil && a.Details.Expiration.Before(now) {
		a.State = model.TERMINATED	// agreement has expired
	}

	if a.State == model.STARTED {
		result, err = EvaluateAgreement(a, cfg)
		if err != nil {
			log.Warn(pathLOG + "[AssessAgreement] Error evaluating agreement " + a.Id + ": " + err.Error())
			return result
		}
		updateAssessment(a, result, now)
	}
	return result
}

// updateAssessment
func updateAssessment(a *model.Agreement, result amodel.Result, now time.Time) {
	if a.Assessment.FirstExecution.IsZero() {
		a.Assessment.FirstExecution = now
	}
	a.Assessment.LastExecution = now

	for _, gt := range a.Details.Guarantees {
		gtname := gt.Name
		last := result.LastValues[gtname]

		violations := []model.Violation{}
		if violated, ok := result.Violated[gtname]; ok {
			violations = violated.Violations
		}
		updateAssessmentGuarantee(a, gtname, last, violations, now)
	}
}

// updateAssessmentGuarantee
func updateAssessmentGuarantee(a *model.Agreement, gtname string, last amodel.ExpressionData,
	violations []model.Violation, now time.Time) {

	ag := a.Assessment.GetGuarantee(gtname)
	ag.LastExecution = now
	if ag.FirstExecution.IsZero() {
		ag.FirstExecution = now
	}
	for _, v := range last {
		ag.LastValues[v.Key] = v
	}
	if len(violations) > 0 {
		ag.LastViolation = &violations[len(violations)-1]
	}
	a.Assessment.SetGuarantee(gtname, ag)
}

/*
EvaluateAgreement evaluates the guarantee terms of an agreement. The metric values are retrieved from a MonitoringAdapter.
The MonitoringAdapter must feed the process correctly (e.g. if the constraint of a guarantee term is of the type "A>B && C>D", the
MonitoringAdapter must supply pairs of values).
*/
func EvaluateAgreement(a *model.Agreement, cfg Config) (amodel.Result, error) {
	ma := cfg.Adapter.Initialize(a)
	now := cfg.Now

	log.Trace(pathLOG+"[EvaluateAgreement] Evaluating Agreement with ID: ", a.Id)
	result := amodel.Result{
		Violated:      map[string]amodel.EvaluationGtResult{},
		LastValues:    map[string]amodel.ExpressionData{},
		LastExecution: map[string]time.Time{},
	}
	gts := a.Details.Guarantees

	for _, gt := range gts {
		/*
		 * TODO Evaluate if gt has to be evaluated according to schedule
		 */
		failed, lastvalues, err := EvaluateGuarantee(a, gt, ma, cfg)
		if err != nil {
			log.Warn("[EvaluateAgreement] Error evaluating expression " + gt.Constraint + ": " + err.Error())
			return amodel.Result{}, err
		}
		if len(failed) > 0 { // VIOLATIONS
			violations := EvaluateGtViolations(a, gt, failed, cfg.Transient)
			gtResult := amodel.EvaluationGtResult{
				Metrics:    failed,
				Violations: violations,
			}
			result.Violated[gt.Name] = gtResult
		} else { // NO VIOLATIONS
			checkWarningInterval(*a, gt, cfg, lastvalues)
			var v model.Violation = model.Violation{
				AgreementId: a.Id,
				Guarantee:   gt.Name,
				//Datetime:       *d,
				//Constraint:     gt.Constraint,
				//Values:         values,
				//ImportanceName: "No violation",
				//Importance:     -1,
				AppId: a.Id, //a.Details.Service?
				//Description:    "",
			}
			updateInternalMetrics(v)
		}
		result.LastValues[gt.Name] = lastvalues
		result.LastExecution[gt.Name] = now
	}
	return result, nil
}

/*
EvaluateGuarantee evaluates a guarantee term of an Agreement (see EvaluateAgreement)
Returns the metrics that failed the GT constraint.
*/
func EvaluateGuarantee(a *model.Agreement, gt model.Guarantee, ma monitor.MonitoringAdapter, cfg Config) (failed []amodel.ExpressionData, last amodel.ExpressionData, err error) {
	log.Debug(pathLOG+"[EvaluateGuarantee] Evaluating Guarantee [" + gt.Name + "] of agreement with ID: ", a.Id)
	failed = make(amodel.GuaranteeData, 0, 1)

	expression, err := govaluate.NewEvaluableExpression(gt.Constraint)
	if err != nil {
		log.Warn(pathLOG+"[EvaluateGuarantee] Error parsing expression '%s'", gt.Constraint)
		return nil, nil, err
	}

	log.Trace(pathLOG+"[EvaluateGuarantee] Getting values from monitor ...")
	values := ma.GetValues(gt, expression.Vars(), cfg.Now)

	if len(values) == 0 {
		log.Warn(pathLOG+"[EvaluateGuarantee] No values found for Guarantee [" + gt.Name + "] of agreement with ID: ", a.Id)
	} else {
		log.Debug(pathLOG+"[EvaluateGuarantee] Total values returned from Monitor [" + a.Id + ", " + gt.Name + "]: ", len(values))
	}

	for _, value := range values {
		aux, err := evaluateExpression(expression, value)
		if err != nil {
			log.Warn("[EvaluateGuarantee] Error evaluating expression " + gt.Constraint + ": " + err.Error())
			return nil, nil, err
		}
		if aux != nil {
			failed = append(failed, aux)
		}
	}
	if len(values) > 0 {
		last = values[len(values)-1]
	}
	return failed, last, nil
}

/*
EvaluateGtViolations creates violations for the detected violated metrics in EvaluateGuarantee
*/
func EvaluateGtViolations(a *model.Agreement, gt model.Guarantee, violated amodel.GuaranteeData, transientTime time.Duration) []model.Violation {
	gtv := make([]model.Violation, 0, len(violated))
	lastViolation := a.Assessment.GetGuarantee(gt.Name).LastViolation

	for _, tuple := range violated {
		// build values map and find newer metric
		var d *time.Time
		var values = make([]model.MetricValue, 0, len(tuple))
		for _, m := range tuple {
			values = append(values, m)
			if d == nil || m.DateTime.After(*d) {
				d = &m.DateTime
			}
		}
		if inTransientTime(*d, lastViolation, transientTime) {
			log.Debug(pathLOG+"[EvaluateGtViolations] Skipping failed metrics %v; last=%s transient=%d newTime=%s", tuple, lastViolation, transientTime, *d)
			continue
		}

		// VIOLATION object
		// with default violation leveles - Importance fields: (intervalName := "Default"), (interval := -1)
		v := model.Violation{
			AgreementId:    a.Id,
			Guarantee:      gt.Name,
			Datetime:       *d,
			Constraint:     gt.Constraint,
			Values:         values,
			ImportanceName: "Default",
			Importance:     -1,
			AppId:          a.Id,
			Description:    "",
		}

		lastViolation = &v	// update last violation value

		// check violation level: e.g., Mild, Serious etc.
		checkViolationLevel(a, &gtv, &v)
	}
	log.Debug(pathLOG+"[EvaluateGtViolations] Violations list content: ", gtv)
	return gtv
}

/*
evaluateExpression evaluate a GT expression at a single point in time with a tuple of metric values (one value per variable in GT expresssion)
The result is: the values if the expression is false (i.e., the failing values), or nil if expression was true
*/
func evaluateExpression(expression *govaluate.EvaluableExpression, values amodel.ExpressionData) (amodel.ExpressionData, error) {
	log.Trace(pathLOG+"[evaluateExpression] Evaluating expression ...")

	evalues := make(map[string]interface{})
	for key, value := range values {
		log.Trace(pathLOG+"[evaluateExpression] key: " + key + ", value: " , value)
		evalues[key] = value.Value
	}
	log.Debug(pathLOG+"[evaluateExpression] (key, value): 	", evalues)

	result, err := expression.Evaluate(evalues)
	if err != nil {
		log.Error(pathLOG + "[evaluateExpression] Error during the evaluation of the expression")
		return nil, err
	}

	log.Trace(pathLOG+"[evaluateExpression] Evaluating expression Vexpression=Vresult with values Vs")
	log.Debug(pathLOG+"[evaluateExpression] Vexpression: 	", expression)
	log.Debug(pathLOG+"[evaluateExpression] Vresult: 		", result)
	log.Trace(pathLOG+"[evaluateExpression] Vs: ", values)

	if _, ok := result.(bool); ok {
		if !result.(bool) {
			return values, nil
		}
	} else {
		log.Warn("[evaluateExpression] 'result' (from evaluation operation) is not a bool object.")

		str := fmt.Sprintf("%v", result)
		if strings.Contains(strings.ToLower(str), "false") {
			log.Debug("[evaluateExpression] 'result' contains a 'false' value.")
			return values, nil
		} else if strings.Contains(strings.ToLower(str), "true") {
			log.Debug("[evaluateExpression] 'result' contains a 'true' value.")
		} else {
			log.Error(pathLOG+"[evaluateExpression] 'result' (from evaluation operation) is not a bool object.")
		}
	}

	return nil, err
}

/*
BuildRetrievalItems returns the RetrievalItems to be passed to an EarlyRetriever.
*/
func BuildRetrievalItems(a *model.Agreement, gt model.Guarantee, varnames []string, to time.Time) []monitor.RetrievalItem {
	result := make([]monitor.RetrievalItem, 0, len(varnames))

	defaultFrom := getDefaultFrom(a, gt)
	for _, name := range varnames {
		v, _ := a.Details.GetVariable(name)
		from := getFromForVariable(v, defaultFrom, to)
		item := monitor.RetrievalItem{
			Guarantee: gt,
			Var:       v,
			From:      from,
			To:        to,
		}
		result = append(result, item)
	}
	return result
}
