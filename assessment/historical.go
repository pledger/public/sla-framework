//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2022
// Updated on 25 Mar 2022
//
// @author: ATOS
//

// Package assessment contains the core code that evaluates the agreements.
package assessment

import (
	"time"
	"strings"
	"strconv"

	assessment_model "atos.pledger/sla-framework/assessment/model"
	log "atos.pledger/sla-framework/common/logs"
	"atos.pledger/sla-framework/model"

	"github.com/lithammer/shortuuid"
)

// 
func storeViolations(agreement model.Agreement, result assessment_model.Result, repo model.IRepository) {
	for k, v := range result.Violated {
		if len(v.Violations) > 0 {
			log.Info(pathLOG + "[storeViolations] Failed guarantee: " + k)
			for _, vi := range v.Violations {
				log.Printf(pathLOG + "[storeViolations] Failed guarantee %v of agreement %s at %s", vi.Guarantee, vi.AgreementId, vi.Datetime)

				vi.Id = generateID(vi.AgreementId)

				//v, err := repo.CreateViolation(&vi)
				repo.CreateViolation(&vi)
			}
		}
	}		
}

/*
generateID generates an Identifier based on a random string and the current time in nanoseconds
*/
func generateID(strPrefix string) string {
	// 1. ID generation
	id := shortuuid.NewWithAlphabet("0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwx")
	id = strings.ToLower(id) // Kubernetes doesnt allow uppercase names for deployments

	// 2. time in nanoseconds is appended to ID
	now := time.Now()      // current local time
	nsec := now.UnixNano() // number of nanoseconds since January 1, 1970 UTC
	id = id + strconv.FormatInt(nsec, 10)

	return id
}