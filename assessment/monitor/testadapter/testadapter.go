//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package testadapter

import (
	"atos.pledger/sla-framework/assessment/monitor"
	"atos.pledger/sla-framework/assessment/monitor/genericadapter"
	"atos.pledger/sla-framework/model"
	"encoding/json"
	"fmt"
	"io"
	"time"
	"math/rand"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "SLA-Framework > Assessment > Monitor > Test Adapter "

const (
	// Name is the unique identifier of this adapter/retriever
	Name = "testadapter"
)

// Retriever implements genericadapter.Retrieve
type Retriever struct {}

/* 
New constructs a Prometheus adapter from a Viper configuration
*/
func New(config *viper.Viper) Retriever {
	logConfig(config)

	return Retriever {}
}

// logConfig
func logConfig(config *viper.Viper) {
	log.Infof(pathLOG+" Configuration loaded.")
}

/* 
Retrieve implements genericadapter.Retrieve
*/
func (r Retriever) Retrieve() genericadapter.Retrieve {
	return func(agreement model.Agreement, items []monitor.RetrievalItem) map[model.Variable][]model.MetricValue {
		log.Info(pathLOG+"[Retrieve] Retrieving metrics from Monitoring-Test Adapter ...")

		result := make(map[model.Variable][]model.MetricValue)
		for _, item := range items {
			log.Info(pathLOG+"[Retrieve] Checking [item.Var.Name=" + item.Var.Name + "] ...")

			// call to test engine
			query := generateRandomResult(item.Var.Name)
			aux := translateVector(query, item.Var.Name)
			result[item.Var] = aux
		}
		log.Infof(pathLOG+" Returning result: %v", result)

		return result
	}
}


// generateRandomResult
/* 	query object example:

	{
		"status": "success",
		"data": {
			"resultType": "vector",
			"result": [
				{
					"metric": {
						"__name__": "go_memstats_frees_total",
						"instance": "localhost:9090",
						"job": "prometheus"
					},
					"value": [
						1571987564.298,
						"629715"
					]
				}
			]
		}
	}
*/
func generateRandomResult(metric string) query {
	data := query{}

	min := 10
    max := 1000
    v := rand.Intn(max - min) + min

	s := string(`{
		"status": "success",
		"data": {
			"resultType": "vector",
			"result": [
				{
					"metric": {
						"__name__": "`+metric+`",
						"instance": "localhost:9090",
						"job": "prometheus"
					},
					"value": [
						1571987564.298,
						"`+strconv.Itoa(v)+`"
					]
				}
			]
		}
	}`)

	err := json.Unmarshal([]byte(s), &data)
	if err != nil {
		log.Error(pathLOG+"Error parsing random data to query struct: ", err.Error())
	}
	
	return data
}

// parse
func parse(r io.Reader, target *query) error {
	return json.NewDecoder(r).Decode(&target)
}

// translateVector
func translateVector(query query, key string) []model.MetricValue {
	res := make([]model.MetricValue, 0, len(query.Data.Results))
	for _, item := range query.Data.Results {
		metric := translateMetric(key, item)
		res = append(res, metric)
	}
	return res
}

// translate2Vectors
func translate2Vectors(query1 query, query2 query, key string) []model.MetricValue {
	res := make([]model.MetricValue, 0, len(query1.Data.Results)+len(query2.Data.Results))
	for _, item := range query1.Data.Results {
		metric := translateMetric(key, item)
		res = append(res, metric)
	}
	for _, item2 := range query2.Data.Results {
		metric := translateMetric(key, item2)
		res = append(res, metric)
	}
	return res
}

// translateMetric 
// TODO this function should be made project-dependent
func translateMetric(key string, item result) model.MetricValue {
	return model.MetricValue{
		Key:      fmt.Sprintf("%s{%s}", key, item.Metric.Instance),
		Value:    item.Item.Value,
		DateTime: time.Time(item.Item.Timestamp),
	}
}
