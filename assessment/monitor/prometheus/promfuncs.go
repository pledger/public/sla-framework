//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

/*
Package prometheus provides a Retriever to get monitoring metrics
from a Prometheus TSDB
*/
package prometheus

import (
	"bytes"
	"os/exec"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

/*
Examples of query:
- NODE AVIALABILITY:
  - curl http://10.99.115.127/api/v1/query -d query="(1 - sum by(node) (sum_over_time(kube_node_status_condition{condition='Ready',status!='true'}[7d])) / (7*24*60)) * 100" | jq

- POD AVIALABILITY:
  - curl http://10.99.115.127/api/v1/query -d query="(1 - sum by(pod) (sum_over_time(kube_pod_status_ready{condition='false'}[7d])) / (7*24*60)) * 100" | jq


Examples of vector output from Prometheus:
- NODE AVIALABILITY:
{
  "status": "success",
  "data": {
    "resultType": "vector",
    "result": [
      {
        "metric": {
          "node": "decenter-integration-2-master"
        },
        "value": [
          1607948601,
          "100"
        ]
      },
      {
        "metric": {
          "node": "decenter-integration-2-slave-cloud-korea-1"
        },
        "value": [
          1607948601,
          "100"
        ]
	  },
	  ... ]}}

- POD AVIALABILITY:
{
  "status": "success",
  "data": {
    "resultType": "vector",
    "result": [
      {
        "metric": {
          "pod": "prometheus-node-exporter-fftc8"
        },
        "value": [
          1607948658.012,
          "100"
        ]
      },
      {
        "metric": {
          "pod": "istio-ingressgateway-6cfd75fc57-dvtdq"
        },
        "value": [
          1607948658.012,
          "100"
        ]
	  },
	  ... ]}}

*/

/*
NodesAvailabilityMetricResponse nodes model
*/
type NodesAvailabilityMetricResponse struct {
	Status string      `json:"status"`
	Data   []dataNodes `json:"data"`
}

type dataNodes struct {
	ResultType string        `json:"resultType"`
	Result     []resultNodes `json:"result"`
}

type resultNodes struct {
	Metric metricNodes `json:"metric"`
	Value  valueNodes  `json:"value"`
}

type metricNodes struct {
	Pod string `json:"pod"`
}

type valueNodes struct {
	Timestamp datetime
	Value     float64
}

/*
PodsAvailabilityMetricResponse nodes model
*/
type PodsAvailabilityMetricResponse struct {
	Status string     `json:"status"`
	Data   []dataPods `json:"data"`
}

type dataPods struct {
	ResultType string       `json:"resultType"`
	Result     []resultPods `json:"result"`
}

type resultPods struct {
	Metric metricPods `json:"metric"`
	Value  valueNodes `json:"value"`
}

type metricPods struct {
	Node string `json:"node"`
}

/*
execCurlQuery get availability of infrastructure nodes
*/
func execCurlQuery(rootURL string, query string, metric string) (string, error) {
	log.Infof("[assessment] [monitor] [prometheus] execCurlQuery > Executing query (curl) ...")
	var stdout, stderr bytes.Buffer

	c := exec.Command(
		"curl",
		"-G",
		"--data-urlencode",
		"query="+query,
		rootURL+"/api/v1/query")

	c.Stdout = &stdout
	c.Stderr = &stderr
	err := c.Run()

	if err != nil {
		log.Error("[assessment] [monitor] [prometheus] execCurlQuery > ERROR executing query [curl -G --data-urlencode query="+query+" "+rootURL+"/api/v1/query: ", err)
		return "", err
	} else {
		log.Infof("[assessment] [monitor] [prometheus] execCurlQuery > curl -G --data-urlencode query=" + query + " " + rootURL + "/api/v1/query")
		str := string(stdout.Bytes())
		log.Infof("[assessment] [monitor] [prometheus] execCurlQuery > Metric: '" + metric + "', Results: " + str)
		return str, nil
	}
}

/*
 */
func parseToQueryObj(str string) query {
	var result query
	log.Infof("[assessment] [monitor] [prometheus] parseToQueryObj > Decoding prometheus .... ")
	err := parse(strings.NewReader(str), &result)
	if err != nil {
		log.Errorf("[assessment] [monitor] [prometheus] parseToQueryObj > Error decoding prometheus output: %s", err.Error())
	}
	return result
}

/*
getNodesAvailability2 get availability of infrastructure nodes
*/
func getNodesAvailability2(rootURL string, metric string, period string, periodMinutes string) (query, error) {
	log.Infof("[assessment] [monitor] [prometheus] getNodesAvailability2 > Retriving metrics from nodes ...")

	// metrics:
	// example: "(1 - sum by(node) (sum_over_time(kube_node_status_condition{condition='Ready',status!='true'}[7d])) / (7*24*60)) * 100"
	qNodesAvailavility := "(1 - sum by(node) (sum_over_time(kube_node_status_condition{condition='Ready',status!='true'}[" + period + "])) / " + periodMinutes + ") * 100"
	log.Info("[assessment] [monitor] [prometheus] getNodesAvailability2 > Nodes availability query: " + qNodesAvailavility)

	// curl:
	str, err := execCurlQuery(rootURL, qNodesAvailavility, metric)
	if err != nil {
		return query{}, err
	}

	// str -> query
	return parseToQueryObj(str), nil
}

/*
getPodsAvailability2 get availability of infrastructure nodes
*/
func getPodsAvailability2(rootURL string, metric string, period string, periodMinutes string) (query, error) {
	log.Infof("[assessment] [monitor] [prometheus] getPodsAvailability2 > Retriving metrics from pods ...")

	// metrics:
	// example: "(1 - sum by (pod) (sum_over_time(kube_pod_status_ready{condition='false'}[7d]))/(7*24*60)) * 100"
	qPodsAvailavility := "(1 - sum by (pod) (sum_over_time(kube_pod_status_ready{condition='false'}[" + period + "])) /" + periodMinutes + ") * 100"
	log.Info("[assessment] [monitor] [prometheus] getPodsAvailability2 >Pods availability query:  " + qPodsAvailavility)

	// curl:
	str, err := execCurlQuery(rootURL, qPodsAvailavility, metric)
	if err != nil {
		return query{}, err
	}

	// str -> query
	return parseToQueryObj(str), nil
}

/*
ProcessMetricsAvailability get and process availability metrics
*/
func ProcessMetricsAvailability(rootURL string, metric string, slaCreationTime time.Time) (query, query) {
	log.Infof("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > Getting and processing metrics ...")

	// get period used in queries
	/*
		ms - milliseconds
		s - seconds
		m - minutes
		h - hours
		d - days - assuming a day has always 24h
		w - weeks - assuming a week has always 7d
		y - years - assuming a year has always 365d
	*/
	//log.Infof("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > slaCreationTime= [" + slaCreationTime.String() + "]")

	t := time.Now()
	//log.Infof("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > time.Now()=      [" + t.String() + "]")

	//diff := t.Sub(slaCreationTime)
	diffMinutes := t.Sub(slaCreationTime).Minutes()

	//sDiffMinutesN := fmt.Sprintf("%f", diffMinutes)
	sDiffMinutes := strconv.Itoa(int(diffMinutes))

	//log.Infof("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > diff=[" + diff.String() + "]; sDiffMinutesN=[" + sDiffMinutesN + "]; sDiffMinutes=[" + sDiffMinutes + "]")

	period := sDiffMinutes + "m"  //"7d"
	periodMinutes := sDiffMinutes //"(7*24*60)"

	//log.Infof("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > period=[" + period + "], periodMinutes=[" + periodMinutes + "]")

	// nodes
	query1, err := getNodesAvailability2(rootURL, metric, period, periodMinutes)
	if err != nil {
		log.Println("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > Error getting nodes information: ", err)
	}

	// pods
	query2, err := getPodsAvailability2(rootURL, metric, period, periodMinutes)
	if err != nil {
		log.Println("[assessment] [monitor] [prometheus] ProcessMetricsAvailability > Error getting pods information: ", err)
	}

	// TODO unir los dos queries
	return query1, query2
}
