//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2022
// Updated on 24 Mar 2022
//
// @author: ATOS
//

/*
Package Monitoring-Engine provides a Retriever to get monitoring metrics from the monitoring engine tool
*/
package monitoringengine

/*
Example of query:
curl 'localhost:9090/api/v1/query?query=hmm_compute_dbdump_time&time=2019-11-14T16:00:00Z'

Example of query range:
curl 'localhost:9090/api/v1/query_range?query=hmm_compute_dbdump_time&start=2019-11-14T16:00:00Z&end=2019-11-14T17:00:00Z&step=15s'

Example of vector output from Prometheus:

	{
		"status": "success",
		"data": {
			"resultType": "vector",
			"result": [
				{
					"metric": {
						"__name__": "go_memstats_frees_total",
						"instance": "localhost:9090",
						"job": "prometheus"
					},
					"value": [
						1571987564.298,
						"629715"
					]
				}
			]
		}
	}
*/

import (
	"atos.pledger/sla-framework/assessment/monitor"
	"atos.pledger/sla-framework/assessment/monitor/genericadapter"
	"atos.pledger/sla-framework/model"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "SLA-Framework > Assessment > Monitor > Monitoring Engine "

const (
	// Name is the unique identifier of this adapter/retriever
	Name = "monitoring-engine"

	// MEngineURLPropertyName is the config property name of the Prometheus URL
	MEngineURLPropertyName = "prometheusUrl"

	// defaultURL is the value of the Prometheus URL is MEngineURLPropertyName is not set
	defaultURL = "http://localhost:9090"

	vectorType resultType = "vector"
	matrixType resultType = "matrix"
)

// Retriever implements genericadapter.Retrieve
type Retriever struct {
	URL string
}

/* 
New constructs a Prometheus adapter from a Viper configuration
*/
func New(config *viper.Viper) Retriever {
	//config.SetDefault(MEngineURLPropertyName, defaultURL)
	logConfig(config)

	return Retriever{
		config.GetString(MEngineURLPropertyName),
	}
}

// logConfig
func logConfig(config *viper.Viper) {
	log.Infof(pathLOG+"Monitoring-Engine configuration:\n"+
		"\tURL: %s", config.GetString(MEngineURLPropertyName))
}

/* 
Retrieve implements genericadapter.Retrieve
*/
func (r Retriever) Retrieve() genericadapter.Retrieve {
	return func(agreement model.Agreement, items []monitor.RetrievalItem) map[model.Variable][]model.MetricValue {
		rootURL := r.prometheusRoot(agreement)
		log.Info(pathLOG+"[Retrieve] Retrieving metrics from Monitoring Engine adapter [" + rootURL + "] ...")

		result := make(map[model.Variable][]model.MetricValue)
		for _, item := range items {
			log.Info(pathLOG+"[Retrieve] Checking [item.Var.Name=" + item.Var.Name + "] ...")

			/*if item.Var.Name == "availability" {
				// availability from nodes and pods
				query1, query2 := ProcessMetricsAvailability(rootURL, item.Var.Metric, agreement.Details.Creation)
				aux := translate2Vectors(query1, query2, item.Var.Name)
				log.Infof("[Retrieve] Call to TranslateVector function for 'query1' & 'query2' results: %v", aux)
				result[item.Var] = aux
			} else {
				// other metrics from prometheus
				url := fmt.Sprintf("%s/api/v1/query?query=%s&time=%s", rootURL, item.Var.Metric, item.To.Format(time.RFC3339))
				query := r.request(url)
				aux := translateVector(query, item.Var.Name)
				result[item.Var] = aux
			}*/

			// call to monitoring engine
			url := fmt.Sprintf("%s/query?query=%s&appId=%s&time=%s", rootURL, item.Var.Metric, agreement.Details.Service, item.To.Format(time.RFC3339))
			query := r.request(url)
			aux := translateVector(query, item.Var.Name)
			result[item.Var] = aux
		}
		log.Infof(pathLOG+" Returning result: %v", result)

		return result
	}
}

// prometheusRoot
func (r Retriever) prometheusRoot(agreement model.Agreement) string {
	if agreement.Assessment.MonitoringURL != "" {
		return agreement.Assessment.MonitoringURL
	}
	return r.URL
}

// request
func (r Retriever) request(url string) query {
	resp, err := http.Get(url)
	if err != nil {
		log.Error(err)
		return query{}
	}
	defer resp.Body.Close()

	var result query
	if resp.Status[0] != '2' { // StatusCode < 200 || resp.StatusCode >= 300
		log.Errorf("%s GET %s", resp.Status, url)
	}
	log.Infof("%d %s", resp.StatusCode, url)
	err = parse(resp.Body, &result)
	if err != nil {
		log.Errorf("[request] Error decoding Monitoring Engine output: %s", err.Error())
	}
	return result
}

// parse
func parse(r io.Reader, target *query) error {
	return json.NewDecoder(r).Decode(&target)
}

// translateVector
func translateVector(query query, key string) []model.MetricValue {
	res := make([]model.MetricValue, 0, len(query.Data.Results))
	for _, item := range query.Data.Results {
		metric := translateMetric(key, item)
		res = append(res, metric)
	}
	return res
}

// translate2Vectors
func translate2Vectors(query1 query, query2 query, key string) []model.MetricValue {
	res := make([]model.MetricValue, 0, len(query1.Data.Results)+len(query2.Data.Results))
	for _, item := range query1.Data.Results {
		metric := translateMetric(key, item)
		res = append(res, metric)
	}
	for _, item2 := range query2.Data.Results {
		metric := translateMetric(key, item2)
		res = append(res, metric)
	}
	return res
}

// translateMetric 
// TODO this function should be made project-dependent
func translateMetric(key string, item result) model.MetricValue {
	return model.MetricValue{
		Key:      fmt.Sprintf("%s{%s}", key, item.Metric.Instance),
		Value:    item.Item.Value,
		DateTime: time.Time(item.Item.Timestamp),
	}
}
