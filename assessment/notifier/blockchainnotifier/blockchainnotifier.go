//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

// Package blockchainnotifier contains the Notifier that notifies the REB about the
// fulfillment or not of an agreement
//
// Usage:
//   url := url.Parse("http://localhost:8080")
//   url.Path = path.Join(url.Path, blockchainnotifier.DefaultRootPath)
//
package blockchainnotifier

import (
	assessment_model "atos.pledger/sla-framework/assessment/model"
	"atos.pledger/sla-framework/assessment/notifier"
	"atos.pledger/sla-framework/connectors/reb"
	"atos.pledger/sla-framework/model"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// BlockchainNotifier is the struct that holds the configuration
// of the blockchain notification
//
// DefaultRootPath contains the default path to the server, which should
// be used to build the BaseURL field, unless the default path has changed
// (e.g. version number of server updated)
type BlockchainNotifier struct {
	rebclient *reb.Client
}

// New constructs a REST Notifier
func New(config *viper.Viper, client *reb.Client) notifier.ViolationNotifier {

	result := BlockchainNotifier{
		rebclient: client,
	}

	return result
}

// NotifyViolations implements ViolationNotifier.NotifyViolations
func (n BlockchainNotifier) NotifyViolations(agreement *model.Agreement, result *assessment_model.Result) {

	if len(result.Violated) == 0 {
		return
	}
	log.Infof("Ǹotifying violations %v on agreement %v", result.Violated, agreement.Id)

	n.rebclient.PatchReservation(agreement.Id, reb.NotFullfilled)
}
