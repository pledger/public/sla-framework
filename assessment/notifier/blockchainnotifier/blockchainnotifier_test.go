//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package blockchainnotifier

import (
	amodel "atos.pledger/sla-framework/assessment/model"
	"atos.pledger/sla-framework/decenter/reb"
	"atos.pledger/sla-framework/model"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path"
	"testing"

	"github.com/spf13/viper"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestNew(t *testing.T) {

	cfg := viper.New()
	cfg.Set(reb.URLPropertyName, "http://localhost:5000")
	client := reb.New(cfg)
	New(cfg, &client)
}

func TestFakeNotifier(t *testing.T) {
	testNotifier(t, false)
	testNotifier(t, true)
}

func testNotifier(t *testing.T, withViolations bool) {
	server := httptest.NewServer(http.HandlerFunc(ok))
	defer server.Close()

	baseURL, _ := url.Parse(server.URL)
	baseURL.Path = path.Join(baseURL.Path, reb.DefaultRootPath)
	n := BlockchainNotifier{
		rebclient: &reb.Client{
			BaseURL: baseURL,
		},
	}

	a := model.Agreement{
		Id: "an-id",
	}
	r := amodel.Result{
		Violated: map[string]amodel.EvaluationGtResult{},
	}

	if withViolations {
		r.Violated["response-time"] = amodel.EvaluationGtResult{
			Violations: []model.Violation{},
		}
	}
	n.NotifyViolations(&a, &r)
}

func ok(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func _404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}
