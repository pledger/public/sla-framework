//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//

/*
To run the integration test

	SLA_REBURL=http://localhost:8080 go test SLALite/assessment/notifier/blockchainnotifier

*/

package blockchainnotifier

import (
	amodel "atos.pledger/sla-framework/assessment/model"
	"atos.pledger/sla-framework/decenter/reb"
	"atos.pledger/sla-framework/model"
	"net/url"
	"os"
	"path"
	"testing"

	log "github.com/sirupsen/logrus"
)

const (
	RebURLEnv = "SLA_REBURL"
)

func checkRunIntegrationTest(t *testing.T) {
	_, ok := os.LookupEnv(RebURLEnv)

	if !ok {
		log.Info("Skipping integration test")
		t.SkipNow()
	}
	log.Info("Running integration test")
}

func rebURL() string {
	url, ok := os.LookupEnv(RebURLEnv)
	if !ok {
		return ""
	}
	return url
}

func TestNotifier(t *testing.T) {
	checkRunIntegrationTest(t)

	baseURL, _ := url.Parse(rebURL())
	baseURL.Path = path.Join(baseURL.Path, reb.DefaultRootPath)
	n := BlockchainNotifier{
		rebclient: &reb.Client{
			BaseURL: baseURL,
		},
	}

	a := model.Agreement{
		Id: "an-id",
	}
	r := amodel.Result{
		Violated: map[string]amodel.EvaluationGtResult{
			"response-time": amodel.EvaluationGtResult{
				Violations: []model.Violation{},
			},
		},
	}
	n.NotifyViolations(&a, &r)
}

func TestNoViolations(t *testing.T) {
	checkRunIntegrationTest(t)

	baseURL, _ := url.Parse(rebURL())
	n := BlockchainNotifier{
		rebclient: &reb.Client{
			BaseURL: baseURL,
		},
	}
	a := model.Agreement{
		Id: "an-id",
	}
	r := amodel.Result{
		Violated: map[string]amodel.EvaluationGtResult{},
	}
	n.NotifyViolations(&a, &r)
}
