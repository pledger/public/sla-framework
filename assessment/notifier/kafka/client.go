//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 12 Jan 2021
//
// @author: ATOS
//
package kafka

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"io/ioutil"
	"strings"
	"time"
	"errors"
	"strconv"

	assessment_model "atos.pledger/sla-framework/assessment/model"
	"atos.pledger/sla-framework/assessment/notifier"
	log "atos.pledger/sla-framework/common/logs"
	"atos.pledger/sla-framework/model"

	"golang.org/x/crypto/pkcs12"
	"github.com/segmentio/kafka-go"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "SLA-Framework > assessment > notifier > Kafka "

/*
KafkaNotifier logs violations
*/
type KafkaNotifier2 struct {
}

// Kafka endpoint server
var KAFKA_ENDPOINT = "localhost:9093"

// Kafka SSL config
var KAFKA_KEYSTORE_LOCATION = "/var/kafka_keystore/kafka.client.keystore.p12"
var KAFKA_KEYSTORE_PASSWORD = ""
var KAFKA_TRUSTSTORE_LOCATION = "/var/kafka_truststore/kafka.client.truststore.pem"
var KAFKA_NOTIFIER_TOPIC = "sla_violation"

// Connection vars
var dialer *kafka.Dialer = nil
var reader *kafka.Reader = nil
var writer *kafka.Writer = nil

// Kafka message counter
var wcounter int

/*
Initialize initializes Kafka client variables
*/
func Initialize() {
	log.Println(pathLOG + "[init] Initializating Kafka client ...")

	wcounter = 0
}

/*
New2 Create a new connection to Kafka server
*/
func New2(config *viper.Viper) notifier.ViolationNotifier {
	KAFKA_ENDPOINT = config.GetString("KAFKA_ENDPOINT")
	KAFKA_KEYSTORE_LOCATION = config.GetString("KAFKA_KEYSTORE_LOCATION")
	KAFKA_KEYSTORE_PASSWORD = config.GetString("KAFKA_KEYSTORE_PASSWORD")
	KAFKA_TRUSTSTORE_LOCATION = config.GetString("KAFKA_TRUSTSTORE_LOCATION")
	KAFKA_NOTIFIER_TOPIC = config.GetString("KAFKA_NOTIFIER_TOPIC")

	dialer = getSSLDialer()
	log.Println(pathLOG + "Kafka Endpoint: " + KAFKA_ENDPOINT)

	return KafkaNotifier2{}
}

/*
CheckForMessages Read messages from Kafka server
*/
func CheckForMessages(topic string) (string, error) {
	return consume(topic)
}

// consume Read from Kafka server
func consume(topic string) (string, error) {
	log.Println(pathLOG + "[consume] Consume [topic=" + topic + "] ...")
	var msg = ""
	if dialer == nil {
		dialer = getSSLDialer()
	}

	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	if reader == nil {
		reader = kafka.NewReader(kafka.ReaderConfig{
			Brokers:   KafkaEndpointList,
			Topic:     topic,
			Partition: 0,
			MinBytes:  10e3, // 10KB
			MaxBytes:  10e6, // 10MB
			Dialer:    dialer,
		})
	}
	reader.SetOffsetAt(context.Background(), time.Now())

	m, err := reader.ReadMessage(context.Background())
	if err != nil {
		log.Error(pathLOG + "[consume] Error reading message: ", err)
		return "", errors.New("Error reading message: " + err.Error())
	}

	msg = string(m.Value)

	return msg, err
}

/*
PublishMessage Write a message to Kafka sever
*/
func PublishMessage(topic string, message string) (string, error) {
	return produce(topic, message)
}

// produce Write to Kafka sever
func produce(topic string, message string) (string, error) {
	wcounter++
	log.Info(pathLOG + "[produce] Producing / sendig  message [topic=" + topic + "] [" + strconv.Itoa(wcounter) + "] ...")

	if dialer == nil {
		dialer = getSSLDialer()
	}

	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	if writer == nil {
		writer = kafka.NewWriter(kafka.WriterConfig{
			Brokers:  KafkaEndpointList,
			Topic:    topic,
			Balancer: &kafka.Hash{},
			Dialer:   dialer,
			WriteTimeout: 30 * time.Second,
			ReadTimeout: 30 * time.Second,
			MaxAttempts: 15,
		})
	}

	err := writer.WriteMessages(context.Background(),
		kafka.Message{
			Key: []byte(strconv.Itoa(wcounter)),
			Value: []byte(message),
		},
	)
	if err != nil {
		log.Error(pathLOG+"[produce] Failed to write messages: ", err)
	}

	if err := writer.Close(); err != nil {
		log.Error(pathLOG+"[produce] Failed to close writer: ", err)
	} else {
		writer = nil
	}
	return "", err

	// TODO check!
	/*
	if err != nil {
		log.Error(pathLOG + "[produce] Failed to write messages: ", err)
		if err2 := writer.Close(); err2 != nil {
			log.Error(pathLOG + "[produce] Failed to close writer: ", err2)
		}
		writer = nil
		return "", err
	}

	if err := writer.Close(); err != nil {
		log.Error(pathLOG + "[produce] Failed to close writer: ", err)
	}
	writer = nil
	return "", err
	*/
}

// SSL Dialer
func getSSLDialer() *kafka.Dialer {
	log.Println(pathLOG + "[getSSLDialer]  Getting SSLDialer...")
	cert := getCertFromKeyStore(KAFKA_KEYSTORE_LOCATION, KAFKA_KEYSTORE_PASSWORD)
	caCert, err := ioutil.ReadFile(KAFKA_TRUSTSTORE_LOCATION)
	if err != nil {
		log.Error(pathLOG + "[getSSLDialer] Error loading CA cert: ", err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	dialer := &kafka.Dialer{
		Timeout:   45 * time.Second,
		DualStack: true,
		TLS: &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
			InsecureSkipVerify: true,
		},
	}
	return dialer
}

// read certs
func getCertFromKeyStore(keystorage string, password string) tls.Certificate {
	log.Printf(pathLOG + "[getCertFromKeyStore] Reading certs ... %s\n", keystorage)
	
	pfxdata, err1 := ioutil.ReadFile(keystorage)
	if err1 != nil {
		log.Error(pathLOG + "[getCertFromKeyStore] Error ReadFile: ", err1)
	}

	blocks, err := pkcs12.ToPEM(pfxdata, password)
	if err != nil {
		log.Error(pathLOG + "[getCertFromKeyStore] Error pkcs12.ToPEM: ", err)
	}

	var pemData []byte
	for _, b := range blocks {
		pemData = append(pemData, pem.EncodeToMemory(b)...)
	}

	// then use PEM data for tls to construct tls certificate:
	cert, err2 := tls.X509KeyPair(pemData, pemData)
	if err2 != nil {
		log.Error(pathLOG + "[getCertFromKeyStore] Error tls.X509KeyPair: ", err2)
	}

	return cert
}

/*
NotifyViolations implements ViolationNotifier interface
*/
func (n KafkaNotifier2) NotifyViolations(agreement *model.Agreement, result *assessment_model.Result) {
	log.Info(pathLOG + "[NotifyViolations] Violation of agreement: " + agreement.Id)
	for k, v := range result.Violated {
		if len(v.Violations) > 0 {
			log.Info(pathLOG + "[NotifyViolations] Failed guarantee: " + k)
			for _, vi := range v.Violations {
				log.Printf(pathLOG + "[NotifyViolations] Failed guarantee %v of agreement %s at %s", vi.Guarantee, vi.AgreementId, vi.Datetime)
				if vi.Id == "" {
					vi.Id = vi.AgreementId
				}
				violationInfo, err := json.Marshal(vi)
				if err != nil {
					log.Error(pathLOG + "[NotifyViolations] Failed to convert to JSON: ", err)
					continue
				}

				msg := strings.ReplaceAll(string(violationInfo), "agreement_id", "sla_id")
				msg = strings.ReplaceAll(msg, "guarantee", "guarantee_id")
				msg = strings.ReplaceAll(msg, "appid", "service_id")
				
				/*
				_, err = produce(KAFKA_NOTIFIER_TOPIC, msg)
				if err != nil {
					log.Error(pathLOG + "[NotifyViolations] Failed to send message: ", err)
					//log.Debug(pathLOG + "[produce] Trying to send message again ...")
					//produce(KAFKA_NOTIFIER_TOPIC, msg)
				}
				*/

				_, err = produce(KAFKA_NOTIFIER_TOPIC, msg)
				if err != nil {
					log.Error(pathLOG+"[NotifyViolations] Error (1) sending SLA violation message to Kafka: ", err)
					log.Println(pathLOG + "[NotifyViolations] Sending again SLA violation message to Kafka ...")
					time.Sleep(15 * time.Second)
					_, err = produce(KAFKA_NOTIFIER_TOPIC, msg)
					if err != nil {
						log.Error(pathLOG+"[NotifyViolations] Error (2) sending message to Kafka: ", err)
					}
				}
			}
		}
	}
}
