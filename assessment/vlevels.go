//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2022
// Updated on 25 Mar 2022
//
// @author: ATOS
//

// Package assessment contains the core code that evaluates the agreements.
package assessment

import (
	"fmt"
	"strings"

	"atos.pledger/sla-framework/model"

	log "atos.pledger/sla-framework/common/logs"
	"github.com/Knetic/govaluate"
)

/*
checkViolationLevel checks and sets violation level
*/
func checkViolationLevel(a *model.Agreement, gtv *[]model.Violation, v *model.Violation) {
	// violation leveles - Importance field
	interval := -1
	violationValue := 0.00

	ps := v	// Pointer to the violation struct type

	// iterate "Importance" to get violation "level"
	for _, i := range a.Details.Guarantees {
		interval = -1
		if i.Name != v.Guarantee {
			continue
		}
		for _, j := range i.Importance {
			expression, err := govaluate.NewEvaluableExpression(fmt.Sprintf("%f", ps.Values[0].Value.(float64)) + j.Constraint)
			if err != nil {
				log.Error(pathLOG + "vlevels > [checkViolationLevel] Error while creating expression")
				continue
			}
			result, err := expression.Evaluate(nil)
			if err != nil {
				log.Error(pathLOG + "vlevels > [checkViolationLevel] Error while evaluating expression")
				continue
			}

			str := fmt.Sprintf("%v", result)
			log.Debug(pathLOG+"vlevels > [checkViolationLevel] " + fmt.Sprintf("%+v", j) + " " +
				"[" + fmt.Sprintf("%f", ps.Values[0].Value.(float64)) + " " + j.Constraint + "]=[" + str + "]")

			if _, ok := result.(bool); ok {
				if result.(bool) {
					violationValue = ps.Values[0].Value.(float64)
					interval++
				}
			} else {
				log.Warn(pathLOG+"vlevels > [checkViolationLevel] 'result' (from evaluation operation) is not a bool object.")

				if strings.Contains(strings.ToLower(str), "false") {
					log.Debug(pathLOG+"vlevels > [checkViolationLevel] 'result' contains a 'false' value.")
				} else if strings.Contains(strings.ToLower(str), "true") {
					log.Debug(pathLOG+"vlevels > [checkViolationLevel] 'result' contains a 'true' value.")
					violationValue = ps.Values[0].Value.(float64)
					interval++
				} else {
					log.Error(pathLOG+"vlevels > [checkViolationLevel] 'result' (from evaluation operation) is not a bool object.")
				}
			}

			switch interval {
			case 0:
				ps.ImportanceName = "Warning"
				ps.Importance = interval
			case 1:
				ps.ImportanceName = "Mild"
				ps.Importance = interval
			case 2:
				ps.ImportanceName = "Serious"
				ps.Importance = interval
			case 3:
				ps.ImportanceName = "Critical"
				ps.Importance = interval
			case 4:
				ps.ImportanceName = "Catastrophic"
				ps.Importance = interval
			default:
				ps.ImportanceName = "Default" 
				ps.Importance = interval
			}
		}

		// add violation to list
		log.Debug(pathLOG+"vlevels > [checkViolationLevel] Violation Value: [" + fmt.Sprintf("%+v", violationValue) +"]; ViolationType: [" + v.ImportanceName + "]")
		*gtv = append(*gtv, *v)

		// update / format content
		updateInternalMetrics(*v)
	}
}