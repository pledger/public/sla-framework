//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2022
// Updated on 25 Mar 2022
//
// @author: ATOS
//

// Package assessment contains the core code that evaluates the agreements.
package assessment

import (
	"fmt"
	"time"
	"strings"

	amodel "atos.pledger/sla-framework/assessment/model"
	metrics "atos.pledger/sla-framework/connectors/metrics"
	"atos.pledger/sla-framework/model"

	log "atos.pledger/sla-framework/common/logs"
	"github.com/Knetic/govaluate"
)

// getDefaultFrom
func getDefaultFrom(a *model.Agreement, gt model.Guarantee) time.Time {
	var defaultFrom = a.Assessment.GetGuarantee(gt.Name).LastExecution
	if defaultFrom.IsZero() {
		defaultFrom = a.Assessment.LastExecution
	}
	if defaultFrom.IsZero() {
		defaultFrom = a.Details.Creation
	}
	return defaultFrom
}

// getFromForVariable returns the interval start for the query to monitoring.
// If the variable is aggregated, it depends on the aggregation window.
// If not, returns defaultFrom (which should be the last time the guarantee term was evaluated)
func getFromForVariable(v model.Variable, defaultFrom, to time.Time) time.Time {
	if v.Aggregation != nil && v.Aggregation.Window != 0 {
		return to.Add(-time.Duration(v.Aggregation.Window) * time.Second)
	}
	return defaultFrom
}

//
//	Prometheus interface for collect internal metrics from a Prometheus job: http://host:port/metrics
//
func updateInternalMetrics(v model.Violation) {
	var intervalValue float64
	var intervalNames []string = []string{"Mild", "Serious", "Severe", "Catastrophic", "No violation"}
	for k := range intervalNames {
		intervalValue = 0
		if v.ImportanceName == intervalNames[k] {
			intervalValue = v.Values[0].Value.(float64)
			metrics.CountViolation(intervalValue,
				map[string]string{
					"application": v.AppId,
					"agreement":   v.AgreementId,
					"metric":      v.Guarantee,
					//"importance":  v.IntervalName,
					"importance": intervalNames[k],
				},
			)
		}
		//metrics.AddSample(v.Values[0].Value.(float64),
		metrics.AddSample(intervalValue,
			map[string]string{
				"application": v.AppId,
				"agreement":   v.AgreementId,
				"metric":      v.Guarantee,
				//"importance":  v.IntervalName,
				"importance": intervalNames[k],
			},
		)
	}
}

//
//	Check guarantee "Warning" interfal if defined
//
func checkWarningInterval(agreement model.Agreement, guarantee model.Guarantee, cfg Config, lastvalue amodel.ExpressionData) {
	not := cfg.Notifier
	for _, j := range guarantee.Importance {
		if j.Name != "Warning" {
			continue
		}
		var value float64
		for _, v := range lastvalue {
			value = v.Value.(float64)
		}

		expression, err := govaluate.NewEvaluableExpression(fmt.Sprintf("%f", value) + j.Constraint)
		if err != nil {
			log.Error(pathLOG + "funcs > [checkWarningInterval] Error while creating expression")
			continue
		}

		result, err := expression.Evaluate(nil)
		if err != nil {
			log.Error(pathLOG + "funcs > [checkWarningInterval] Error while evaluating expression")
			continue
		}

		if _, ok := result.(bool); ok {
			if result.(bool) {
				var result2 amodel.Result
				var evalgtresult amodel.EvaluationGtResult
				var violation = model.Violation{
					//Id:             agreement.Id,
					AgreementId:    agreement.Id,
					Guarantee:      guarantee.Name,
					Datetime:       cfg.Now,
					Constraint:     guarantee.Constraint,
					Values:         []model.MetricValue{},
					ImportanceName: "Warning",
					Importance:     -1,
					AppId:          agreement.Details.Service,
					Description:    "",
				}
				evalgtresult.Violations = append(evalgtresult.Violations, violation)
				result2.Violated = make(map[string]amodel.EvaluationGtResult)
				result2.Violated[guarantee.Name] = evalgtresult
				if not != nil {
					not.NotifyViolations(&agreement, &result2)
				}
			}
		} else {
			log.Warn("funcs > [checkWarningInterval] 'result' (from evaluation operation) is not a bool object.")
	
			str := fmt.Sprintf("%v", result)
			if strings.Contains(strings.ToLower(str), "false") {
				log.Debug("funcs > [checkWarningInterval] 'result' contains a 'false' value.")
			} else if strings.Contains(strings.ToLower(str), "true") {
				log.Debug("funcs > [checkWarningInterval] 'result' contains a 'true' value.")

				var result2 amodel.Result
				var evalgtresult amodel.EvaluationGtResult
				var violation = model.Violation{
					//Id:             agreement.Id,
					AgreementId:    agreement.Id,
					Guarantee:      guarantee.Name,
					Datetime:       cfg.Now,
					Constraint:     guarantee.Constraint,
					Values:         []model.MetricValue{},
					ImportanceName: "Warning",
					Importance:     -1,
					AppId:          agreement.Details.Service,
					Description:    "",
				}
				evalgtresult.Violations = append(evalgtresult.Violations, violation)
				result2.Violated = make(map[string]amodel.EvaluationGtResult)
				result2.Violated[guarantee.Name] = evalgtresult
				if not != nil {
					not.NotifyViolations(&agreement, &result2)
				}
			} else {
				log.Error(pathLOG+"funcs > [checkWarningInterval] 'result' (from evaluation operation) is not a bool object.")
			}
		}
	}
}

// inTransientTime returns if the new violation detected occurs in the transient time
// of the guarantee term; i.e. last + transient < newviolation
func inTransientTime(newViolation time.Time, last *model.Violation, transientTime time.Duration) bool {
	// first violations are always considered out of the transient time
	if last == nil {
		return false
	}
	return newViolation.Before(last.Datetime.Add(transientTime))
}