//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 2017
// Updated on 23 Mar 2022
//
// @author: ATOS
//
package main

import (
	"atos.pledger/sla-framework/assessment"
	"atos.pledger/sla-framework/assessment/monitor"
	"atos.pledger/sla-framework/assessment/monitor/genericadapter"
	"atos.pledger/sla-framework/assessment/monitor/prometheus"
	"atos.pledger/sla-framework/assessment/monitor/testadapter"
	monitoring_engine "atos.pledger/sla-framework/assessment/monitor/monitoring-engine"
	"atos.pledger/sla-framework/assessment/notifier"
	"atos.pledger/sla-framework/assessment/notifier/blockchainnotifier"
	kafkaNotifier "atos.pledger/sla-framework/assessment/notifier/kafka"
	"atos.pledger/sla-framework/assessment/notifier/lognotifier"
	log "atos.pledger/sla-framework/common/logs"
	kafkaConnector "atos.pledger/sla-framework/connectors/kafka"
	"atos.pledger/sla-framework/connectors/mqtt"
	"atos.pledger/sla-framework/connectors/reb"
	metrics "atos.pledger/sla-framework/connectors/metrics"
	"atos.pledger/sla-framework/model"
	"atos.pledger/sla-framework/repositories/memrepository"
	"atos.pledger/sla-framework/repositories/mongodb"
	"atos.pledger/sla-framework/repositories/validation"
	"atos.pledger/sla-framework/utils"

	"os"
	"strconv"
	"time"
	"strings"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "SLA-Framework "

/*
Main function. App entrypoint: main.go and app.go
*/
func main() {
	// Set environment variables used during tests:
	// prometheus:
	//os.Setenv(utils.AdapterTypePropertyName, "prometheus")
	//os.Setenv(prometheus.PrometheusURLPropertyName, "http://192.168.1.141:32659")
	// monitoring-engine:
	//os.Setenv(utils.AdapterTypePropertyName, "monitoring-engine")
	//os.Setenv(prometheus.MEngineURLPropertyName, "http://192.168.1.141:32659")
	// test:
	os.Setenv(utils.AdapterTypePropertyName, "testadapter")

	log.Info("####################################################################################")
	log.Info(pathLOG + "Starting Pledger SLA Framework (v0.3.0.20220329-1) ...")

	// configuration
	config := createMainConfig()
	logMainConfig(config)

	singlefile := config.GetBool(utils.SingleFilePropertyName)
	checkPeriod := asSeconds(config, utils.CheckPeriodPropertyName)
	repoType := config.GetString(utils.RepositoryTypePropertyName)
	trasientTime := asSeconds(config, utils.TransientTimePropertyName)

	utils.AddTrustedCAs(config)

	var repoconfig *viper.Viper
	if singlefile {
		repoconfig = config
	}

	log.Info(pathLOG + "Setting Database Adapter ...")
	var repo model.IRepository
	var errRepo error
	switch repoType {
	case utils.DefaultRepositoryType, "":
		log.Info(pathLOG + "Using default Database Adapter [memory repository] ... ")
		repo, errRepo = memrepository.New(repoconfig)
	case "mongodb":
		log.Info(pathLOG + "Using MongoDB Database Adapter [MongoDB repository] ... ")
		repo, errRepo = mongodb.New(repoconfig)
	}
	if errRepo != nil {
		log.Fatal(pathLOG+"Error creating repository: ", errRepo.Error())
	}

	validater := model.NewDefaultValidator(config.GetBool(utils.ExternalIDsPropertyName), true)

	adapter := buildAdapter(config)

	repo, _ = validation.New(repo, validater)

	var rebClient reb.Client
	var notifier notifier.ViolationNotifier
	var mqttSubscriber mqtt.Subscriber

	log.Info(pathLOG + "Setting Notifier and Subscriber ...")
	if config.GetString(reb.URLPropertyName) != "" {
		log.Info(pathLOG + "Using REB Notifier and MQTT Subscriber ...")
		rebClient = reb.New(config)
		notifier = blockchainnotifier.New(config, &rebClient)
		if config.GetString(mqtt.URLPropertyName) != "" {
			handler, err := mqtt.NewHandler(config, &rebClient, repo, validater)
			if err != nil {
				log.Fatal(err)
			}
			mqttSubscriber = mqtt.New(config, handler.Callback)
			go func() {
				mqttSubscriber.Subscribe()
			}()
		} else {
			log.Error(pathLOG+"Error setting REB Notifier and MQTT Subscriber")
		}
	} else {
		notifierType := config.GetString("NOTIFIER")
		switch notifierType {
		case "kafka", "kafka2":
			log.Info(pathLOG + "Using KAFKA Notifier and Subscriber ...")

			kafkaNotifier.Initialize()
			notifier = kafkaNotifier.New2(config)

			// Kafka events handler
			log.Println(pathLOG + "Initializing event handler ...")
			kafkaConnector.NewEventHandler()
		default:
			log.Info(pathLOG + "Using Default Notifier (no subscriber) ...")

			notifier = lognotifier.LogNotifier{}
		}
	}

	if repo != nil {
		a, _ := NewApp(config, repo, validater)
		aCfg := assessment.Config{
			Repo:      repo,
			Adapter:   adapter,
			Notifier:  notifier,
			Transient: trasientTime,
		}
		go createValidationThread(checkPeriod, aCfg)

		log.Info(pathLOG + "Initializing internal metrics...")
		//go metrics.Init()
		metrics.Init()
		a.Router.Methods("GET").Path("/metrics").Handler(promhttp.Handler())
		log.Info(pathLOG + "Internal metrics correctly initialized.")

		a.Run()
	} else {
		log.Error(pathLOG+"Error starting SLA evaluation process")
	}
}

// 
func buildAdapter(config *viper.Viper) monitor.MonitoringAdapter {
	aType := config.GetString(utils.AdapterTypePropertyName)
	if os.Getenv(utils.AdapterTypePropertyName) == prometheus.Name {
		aType = prometheus.Name
	} else if os.Getenv(utils.AdapterTypePropertyName) == monitoring_engine.Name {
		aType = monitoring_engine.Name
	} else if os.Getenv(utils.AdapterTypePropertyName) == testadapter.Name {
		aType = testadapter.Name
	}

	switch aType {
	case monitoring_engine.Name:
		adapter := genericadapter.New(
			monitoring_engine.New(config).Retrieve(),
			genericadapter.Identity)
		return adapter
	case prometheus.Name:
		adapter := genericadapter.New(
			prometheus.New(config).Retrieve(),
			genericadapter.Identity)
		return adapter
	case testadapter.Name:
		adapter := genericadapter.New(
			testadapter.New(config).Retrieve(),
			genericadapter.Identity)
		return adapter
	default:
		adapter := genericadapter.New(
			genericadapter.DummyRetriever{Size: 3}.Retrieve(),
			genericadapter.Identity)
		return adapter
	}

}

// 
func asSeconds(config *viper.Viper, field string) time.Duration {
	raw := config.GetString(field)
	// if it is already a valid duration, return directly
	if _, err := time.ParseDuration(raw); err == nil {
		return config.GetDuration(field)
	}

	// if not, assume it is (decimal) number of seconds; read as ms and convert to seconds.
	ms := config.GetFloat64(field)
	return time.Duration(ms*1000) * time.Millisecond
}

//
func shortDur(d time.Duration) string {
    s := d.String()
    if strings.HasSuffix(s, "m0s") {
        s = s[:len(s)-2]
    }
    if strings.HasSuffix(s, "h0m") {
        s = s[:len(s)-2]
    }
    return s
}

//
// Creates the main Viper configuration.
// file: if set, is the path to a configuration file. If not set, paths and basename will be used
// paths: colon separated paths where to search a config file
// basename: basename of a configuration file accepted by Viper (extension is automatic)
//
func createMainConfig() *viper.Viper {
	log.Info(pathLOG + "Generating SLA Framework configuration values ...")

	log.Info(pathLOG + "  Getting SLA Framework environment variables...")
	log.Debug(pathLOG + "    REB endpoint ............... " + os.Getenv(reb.URLPropertyName))
	log.Debug(pathLOG + "    REB path ................... " + os.Getenv(reb.PathPropertyName))
	log.Debug(pathLOG + "    MQTT Broker ................ " + os.Getenv(mqtt.URLPropertyName))
	log.Debug(pathLOG + "    MQTT Topic ................. " + os.Getenv(mqtt.TopicPropertyName))
	log.Debug(pathLOG + "    Prometheus Endpoint ........ " + os.Getenv(prometheus.PrometheusURLPropertyName))
	log.Debug(pathLOG + "    MEngine Endpoint ........... " + os.Getenv(monitoring_engine.MEngineURLPropertyName))
	log.Debug(pathLOG + "    Database Adapter ........... " + os.Getenv(utils.AdapterTypePropertyName))
	log.Debug(pathLOG + "    KAFKA_CLIENT ............... " + os.Getenv("KAFKA_CLIENT"))
	log.Debug(pathLOG + "    KAFKA_AGREEMENT_TOPIC ...... " + os.Getenv("KAFKA_AGREEMENT_TOPIC"))
	log.Debug(pathLOG + "    KAFKA_NOTIFIER_TOPIC ....... " + os.Getenv("KAFKA_NOTIFIER_TOPIC"))
	log.Debug(pathLOG + "    KAFKA_KEYSTORE_LOCATION .... " + os.Getenv("KAFKA_KEYSTORE_LOCATION"))
	log.Debug(pathLOG + "    KAFKA_KEYSTORE_PASSWORD .... " + os.Getenv("KAFKA_KEYSTORE_PASSWORD"))
	log.Debug(pathLOG + "    KAFKA_TRUSTSTORE_LOCATION .. " + os.Getenv("KAFKA_TRUSTSTORE_LOCATION"))
	log.Debug(pathLOG + "    CONFIGSVC_ENDPOINT ......... " + os.Getenv("CONFIGSVC_ENDPOINT"))
	log.Debug(pathLOG + "    CONFIGSVC_CREDENTIALS ...... " + os.Getenv("CONFIGSVC_CREDENTIALS"))

	config := viper.New()

	config.SetEnvPrefix(utils.ConfigPrefix) // Env vars start with 'SLA_'
	config.AutomaticEnv()

	// SLA configuration
	log.Info(pathLOG + "  Setting configuration values ...")
	config.SetDefault(utils.CheckPeriodPropertyName, utils.DefaultCheckPeriod)
	config.SetDefault(utils.RepositoryTypePropertyName, utils.DefaultRepositoryType)
	if os.Getenv(utils.AdapterTypePropertyName) == "" {
		config.SetDefault(utils.AdapterTypePropertyName, utils.DefaultAdapterType)
	} else {
		config.SetDefault(utils.AdapterTypePropertyName, os.Getenv(utils.AdapterTypePropertyName))
	}
	config.SetDefault(utils.ExternalIDsPropertyName, utils.DefaultExternalIDs)

	// REB
	config.SetDefault(reb.URLPropertyName, os.Getenv(reb.URLPropertyName))
	config.SetDefault(reb.PathPropertyName, os.Getenv(reb.PathPropertyName))

	// MQTT
	config.SetDefault(mqtt.URLPropertyName, os.Getenv(mqtt.URLPropertyName))
	config.SetDefault(mqtt.TopicPropertyName, os.Getenv(mqtt.TopicPropertyName))

	// Monitoring System
	if os.Getenv(prometheus.PrometheusURLPropertyName) == "" {
		config.SetDefault(prometheus.PrometheusURLPropertyName, "http://localhost:9090")
	} else {
		config.SetDefault(prometheus.PrometheusURLPropertyName, os.Getenv(prometheus.PrometheusURLPropertyName))
	}
	if os.Getenv(monitoring_engine.MEngineURLPropertyName) == "" {
		config.SetDefault(monitoring_engine.MEngineURLPropertyName, "http://localhost:9090")
	} else {
		config.SetDefault(monitoring_engine.MEngineURLPropertyName, os.Getenv(monitoring_engine.MEngineURLPropertyName))
	}

	config.SetDefault(utils.TransientTimePropertyName, utils.DefaultTransientTime)

	log.Info(pathLOG + "  Returning configuration objrct ...")
	return config
}

// 
func logMainConfig(config *viper.Viper) {
	log.Infof(pathLOG+"[logMainConfig] Loading initial configuration values ... ")

	checkPeriod := asSeconds(config, utils.CheckPeriodPropertyName)
	repoType := config.GetString(utils.RepositoryTypePropertyName)
	adapterType := config.GetString(utils.AdapterTypePropertyName)
	externalIDs := config.GetBool(utils.ExternalIDsPropertyName)
	rebURL := config.GetString(reb.URLPropertyName)
	rebPath := config.GetString(reb.PathPropertyName)
	brokerURL := config.GetString(mqtt.URLPropertyName)
	brokerTopic := config.GetString(mqtt.TopicPropertyName)
	promURL := config.GetString(prometheus.PrometheusURLPropertyName)
	mEngine := config.GetString(monitoring_engine.MEngineURLPropertyName)
	transientTime := asSeconds(config, utils.TransientTimePropertyName)
	KAFKA_KEYSTORE_LOCATION := config.GetString("KAFKA_KEYSTORE_LOCATION")
	KAFKA_AGREEMENT_TOPIC := config.GetString("KAFKA_AGREEMENT_TOPIC")
	KAFKA_NOTIFIER_TOPIC := config.GetString("KAFKA_NOTIFIER_TOPIC")
	CONFIGSVC_ENDPOINT := config.GetString("CONFIGSVC_ENDPOINT")

	log.Infof(pathLOG+"[logMainConfig] SLALite initialization values\n"+
		"\tConfigfile: " + config.ConfigFileUsed() + "\n"+
		"\tRepository type: " + repoType + "\n"+
		"\tAdapter type: " + adapterType + "\n"+
		"\tExternal IDs: " + strconv.FormatBool(externalIDs) + "\n"+
		"\tCheck period: " + shortDur(checkPeriod) + "\n"+
		"\tCONNECTORS:\n"+
		"\t\tREB\n"+
		"\t\t\tREB URL: " + rebURL + "\n"+
		"\t\t\tREB path: " + rebPath + "\n"+
		"\t\tMQTT BROKER\n"+
		"\t\t\tBroker URL: " + brokerURL + "\n"+
		"\t\t\tBroker topic: " + brokerTopic + "\n"+
		"\t\tPROMETHEUS\n"+
		"\t\t\tPrometheus URL: " + promURL + "\n"+
		"\t\tMONITORING-ENGINE\n"+
		"\t\t\tURL: " + mEngine + "\n"+
		"\t\tKAFKA\n"+
		"\t\t\tKAFKA KEYSTORE Location: " + KAFKA_KEYSTORE_LOCATION + "\n"+
		"\t\t\tKAFKA AGREEMENT Topic: " + KAFKA_AGREEMENT_TOPIC + "\n"+
		"\t\t\tKAFKA NOTIFIER Topic: " + KAFKA_NOTIFIER_TOPIC + "\n"+
		"\t\tCONFIGSVC\n"+
		"\t\t\tCONFIGSVC_ENDPOINT: " + CONFIGSVC_ENDPOINT + "\n"+
		"\tTransient time: " + shortDur(transientTime) + "\n"+
		"\tCheck period: " + shortDur(checkPeriod) + "\n")

	caPath := config.GetString(utils.CAPathPropertyName)
	if caPath != "" {
		log.Infof(pathLOG+"[logMainConfig] SLALite intialization. Trusted CAs file: %s", caPath)
	}
}

//
func createValidationThread(checkPeriod time.Duration, cfg assessment.Config) {
	ticker := time.NewTicker(checkPeriod)

	for {
		<-ticker.C
		cfg.Now = time.Now()
		assessment.AssessActiveAgreements(cfg)
	}
}

// 
func validateProviders(repo model.IRepository) {
	providers, err := repo.GetAllProviders()

	if err == nil {
		log.Println(pathLOG + "[validateProviders] There are " + strconv.Itoa(len(providers)) + " providers")
	} else {
		log.Println(pathLOG + "[validateProviders] Error: " + err.Error())
	}
}
